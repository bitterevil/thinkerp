<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wChangePass
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wChangePass))
        Me.olaPwdConfirm = New System.Windows.Forms.Label()
        Me.otbPwdConfirm = New System.Windows.Forms.TextBox()
        Me.olaPwdNew = New System.Windows.Forms.Label()
        Me.otbPwdNew = New System.Windows.Forms.TextBox()
        Me.olaPwdOld = New System.Windows.Forms.Label()
        Me.ocmCancel = New System.Windows.Forms.Button()
        Me.olaCode = New System.Windows.Forms.Label()
        Me.otbCodeNew = New System.Windows.Forms.TextBox()
        Me.otbPwdOld = New System.Windows.Forms.TextBox()
        Me.ocmOk = New System.Windows.Forms.Button()
        Me.ogbForm = New System.Windows.Forms.GroupBox()
        Me.opbLogin = New System.Windows.Forms.PictureBox()
        Me.opnForm = New System.Windows.Forms.Panel()
        Me.ogbForm.SuspendLayout()
        CType(Me.opbLogin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.opnForm.SuspendLayout()
        Me.SuspendLayout()
        '
        'olaPwdConfirm
        '
        Me.olaPwdConfirm.AutoSize = True
        Me.olaPwdConfirm.BackColor = System.Drawing.Color.Transparent
        Me.olaPwdConfirm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaPwdConfirm.Location = New System.Drawing.Point(13, 150)
        Me.olaPwdConfirm.Name = "olaPwdConfirm"
        Me.olaPwdConfirm.Size = New System.Drawing.Size(116, 16)
        Me.olaPwdConfirm.TabIndex = 9
        Me.olaPwdConfirm.Tag = "2;�׹�ѹ���ʼ�ҹ;Confirm Password"
        Me.olaPwdConfirm.Text = "Confirm Password"
        '
        'otbPwdConfirm
        '
        Me.otbPwdConfirm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbPwdConfirm.Location = New System.Drawing.Point(135, 147)
        Me.otbPwdConfirm.MaxLength = 20
        Me.otbPwdConfirm.Name = "otbPwdConfirm"
        Me.otbPwdConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.otbPwdConfirm.Size = New System.Drawing.Size(205, 22)
        Me.otbPwdConfirm.TabIndex = 3
        '
        'olaPwdNew
        '
        Me.olaPwdNew.AutoSize = True
        Me.olaPwdNew.BackColor = System.Drawing.Color.Transparent
        Me.olaPwdNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaPwdNew.Location = New System.Drawing.Point(13, 122)
        Me.olaPwdNew.Name = "olaPwdNew"
        Me.olaPwdNew.Size = New System.Drawing.Size(98, 16)
        Me.olaPwdNew.TabIndex = 8
        Me.olaPwdNew.Tag = "2;���ʼ�ҹ����;New Password"
        Me.olaPwdNew.Text = "New Password"
        '
        'otbPwdNew
        '
        Me.otbPwdNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbPwdNew.Location = New System.Drawing.Point(135, 119)
        Me.otbPwdNew.MaxLength = 20
        Me.otbPwdNew.Name = "otbPwdNew"
        Me.otbPwdNew.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.otbPwdNew.Size = New System.Drawing.Size(205, 22)
        Me.otbPwdNew.TabIndex = 2
        '
        'olaPwdOld
        '
        Me.olaPwdOld.AutoSize = True
        Me.olaPwdOld.BackColor = System.Drawing.Color.Transparent
        Me.olaPwdOld.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaPwdOld.Location = New System.Drawing.Point(13, 94)
        Me.olaPwdOld.Name = "olaPwdOld"
        Me.olaPwdOld.Size = New System.Drawing.Size(92, 16)
        Me.olaPwdOld.TabIndex = 7
        Me.olaPwdOld.Tag = "2;���ʼ�ҹ���;Old Password"
        Me.olaPwdOld.Text = "Old Password"
        '
        'ocmCancel
        '
        Me.ocmCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ocmCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmCancel.Location = New System.Drawing.Point(241, 175)
        Me.ocmCancel.Name = "ocmCancel"
        Me.ocmCancel.Size = New System.Drawing.Size(100, 30)
        Me.ocmCancel.TabIndex = 5
        Me.ocmCancel.Tag = "2;�Դ;Close"
        Me.ocmCancel.Text = "Close"
        Me.ocmCancel.UseVisualStyleBackColor = True
        '
        'olaCode
        '
        Me.olaCode.AutoSize = True
        Me.olaCode.BackColor = System.Drawing.Color.Transparent
        Me.olaCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaCode.Location = New System.Drawing.Point(13, 66)
        Me.olaCode.Name = "olaCode"
        Me.olaCode.Size = New System.Drawing.Size(103, 16)
        Me.olaCode.TabIndex = 6
        Me.olaCode.Tag = "2;���ʼ��������;New User Code"
        Me.olaCode.Text = "New User Code"
        '
        'otbCodeNew
        '
        Me.otbCodeNew.BackColor = System.Drawing.Color.White
        Me.otbCodeNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbCodeNew.Location = New System.Drawing.Point(135, 63)
        Me.otbCodeNew.MaxLength = 20
        Me.otbCodeNew.Name = "otbCodeNew"
        Me.otbCodeNew.Size = New System.Drawing.Size(205, 22)
        Me.otbCodeNew.TabIndex = 0
        '
        'otbPwdOld
        '
        Me.otbPwdOld.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.otbPwdOld.Location = New System.Drawing.Point(136, 91)
        Me.otbPwdOld.MaxLength = 20
        Me.otbPwdOld.Name = "otbPwdOld"
        Me.otbPwdOld.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.otbPwdOld.Size = New System.Drawing.Size(205, 22)
        Me.otbPwdOld.TabIndex = 1
        '
        'ocmOk
        '
        Me.ocmOk.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmOk.Location = New System.Drawing.Point(135, 175)
        Me.ocmOk.Name = "ocmOk"
        Me.ocmOk.Size = New System.Drawing.Size(100, 30)
        Me.ocmOk.TabIndex = 4
        Me.ocmOk.Tag = "2;�ѹ�֡;Save"
        Me.ocmOk.Text = "Save"
        Me.ocmOk.UseVisualStyleBackColor = True
        '
        'ogbForm
        '
        Me.ogbForm.BackColor = System.Drawing.Color.Transparent
        Me.ogbForm.Controls.Add(Me.olaPwdConfirm)
        Me.ogbForm.Controls.Add(Me.otbPwdConfirm)
        Me.ogbForm.Controls.Add(Me.olaPwdNew)
        Me.ogbForm.Controls.Add(Me.otbPwdNew)
        Me.ogbForm.Controls.Add(Me.ocmOk)
        Me.ogbForm.Controls.Add(Me.opbLogin)
        Me.ogbForm.Controls.Add(Me.otbPwdOld)
        Me.ogbForm.Controls.Add(Me.olaPwdOld)
        Me.ogbForm.Controls.Add(Me.ocmCancel)
        Me.ogbForm.Controls.Add(Me.olaCode)
        Me.ogbForm.Controls.Add(Me.otbCodeNew)
        Me.ogbForm.Location = New System.Drawing.Point(9, 3)
        Me.ogbForm.Name = "ogbForm"
        Me.ogbForm.Size = New System.Drawing.Size(355, 219)
        Me.ogbForm.TabIndex = 0
        Me.ogbForm.TabStop = False
        '
        'opbLogin
        '
        Me.opbLogin.BackColor = System.Drawing.Color.Transparent
        Me.opbLogin.Image = CType(resources.GetObject("opbLogin.Image"), System.Drawing.Image)
        Me.opbLogin.Location = New System.Drawing.Point(16, 14)
        Me.opbLogin.Name = "opbLogin"
        Me.opbLogin.Size = New System.Drawing.Size(48, 48)
        Me.opbLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.opbLogin.TabIndex = 4
        Me.opbLogin.TabStop = False
        '
        'opnForm
        '
        Me.opnForm.Controls.Add(Me.ogbForm)
        Me.opnForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnForm.Location = New System.Drawing.Point(0, 0)
        Me.opnForm.Name = "opnForm"
        Me.opnForm.Size = New System.Drawing.Size(374, 232)
        Me.opnForm.TabIndex = 0
        '
        'wChangePass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ocmCancel
        Me.ClientSize = New System.Drawing.Size(374, 232)
        Me.Controls.Add(Me.opnForm)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wChangePass"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;����¹����;Change Password"
        Me.Text = "Change Password"
        Me.ogbForm.ResumeLayout(False)
        Me.ogbForm.PerformLayout()
        CType(Me.opbLogin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.opnForm.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents olaPwdConfirm As System.Windows.Forms.Label
    Friend WithEvents otbPwdConfirm As System.Windows.Forms.TextBox
    Friend WithEvents olaPwdNew As System.Windows.Forms.Label
    Friend WithEvents otbPwdNew As System.Windows.Forms.TextBox
    Friend WithEvents olaPwdOld As System.Windows.Forms.Label
    Friend WithEvents ocmCancel As System.Windows.Forms.Button
    Friend WithEvents olaCode As System.Windows.Forms.Label
    Friend WithEvents otbCodeNew As System.Windows.Forms.TextBox
    Friend WithEvents otbPwdOld As System.Windows.Forms.TextBox
    Friend WithEvents ocmOk As System.Windows.Forms.Button
    Friend WithEvents ogbForm As System.Windows.Forms.GroupBox
    Friend WithEvents opbLogin As System.Windows.Forms.PictureBox
    Friend WithEvents opnForm As System.Windows.Forms.Panel
End Class
