Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Image
Imports Microsoft.VisualBasic
Imports System.Drawing.Imaging
Imports Microsoft.VisualBasic.Compatibility
Imports C1.Win.C1FlexGrid


Public Class wBrowse

#Region " VARIABLE "

    'Private cnn As New SqlConnection
    Private oW_Cmd As New SqlCommand
    Private oW_Adp As New SqlDataAdapter
    Private tW_ConnectString As String
    Private oW_DsFilter As New DataSet
    Private oW_DvFilter As New DataView
    Private oW_DtFilter As New DataTable
    Private tCaptionForm As String = ""
    Private tCommandText As String = ""
    Private tTableName As String = ""
    Private tCaptionName As String = ""
    Private tColumnName As String = ""
    Private tCommandLenth As String = ""
    Private tColumnLenth As String = ""
    Private nDefaultColumn As Integer = 1
    Private tAlignMent As String = ""
    Private tFedSort As String = ""
    Private oW_DrReturn As DataRow
    Private bReturn As Boolean = False

    Private tCaptionCol As String = ""

    Private tW_FormName As String = ""

#End Region

#Region " PROPERTY "

    '<Description("Return DataRow"), Category("Ada Soft Co.,Ltd")> _
    Public Property mReturnDatarow() As DataRow
        Get
            Return oW_DrReturn
        End Get
        Set(ByVal value As DataRow)

        End Set
    End Property

    '<Description("Return Field Selected"), Category("Ada Soft Co.,Ltd")> _
    Public Property mReturnSelected() As Boolean
        Get
            Return bReturn
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    '<Description("Set Caption Form"), Category("Ada Soft Co.,Ltd")> _
    Public Property mCaptionForm() As String
        Get
            Return tCaptionForm
        End Get
        Set(ByVal value As String)
            tCaptionForm = value
        End Set
    End Property

    '<Description("Sqlcommand Text"), Category("Ada Soft Co.,Ltd")> _
    Public Property mCommandText() As String
        Get
            Return tCommandText
        End Get
        Set(ByVal value As String)
            tCommandText = value
        End Set
    End Property

    '<Description("Set Table"), Category("Ada Soft Co.,Ltd")> _
    Public Property mTableName() As String
        Get
            Return tTableName
        End Get
        Set(ByVal value As String)
            tTableName = value
        End Set
    End Property

    '<Description("Set Column"), Category("Ada Soft Co.,Ltd")> _
    Public Property mColumnName() As String
        Get
            Return tColumnName
        End Get
        Set(ByVal value As String)
            tColumnName = value
        End Set
    End Property

    '<Description(""), Category("Ada Soft Co.,Ltd")> _
    Public Property mCaptionName() As String
        Get
            Return tCaptionName
        End Get
        Set(ByVal value As String)
            tCaptionName = value
        End Set
    End Property

    '<Description(""), Category("Ada Soft Co.,Ltd")> _
    Public Property mCommandLenth() As String
        Get
            Return tCommandLenth
        End Get
        Set(ByVal value As String)
            tCommandLenth = value
        End Set
    End Property

    '<Description(""), Category("Ada Soft Co.,Ltd")> _
    Public Property mColumnLenth() As String
        Get
            Return tColumnLenth
        End Get
        Set(ByVal value As String)
            tColumnLenth = value
        End Set
    End Property

    '<Description(""), Category("Ada Soft Co.,Ltd")> _
    Public Property mDefaultColumn() As Integer
        Get
            Return nDefaultColumn
        End Get
        Set(ByVal value As Integer)
            nDefaultColumn = value
        End Set
    End Property

    '<Description(""), Category("Ada Soft Co.,Ltd")> _
    Public Property mAlignMent() As String
        Get
            Return tAlignMent
        End Get
        Set(ByVal value As String)
            tAlignMent = value
        End Set
    End Property

    '<Description(""), Category("Ada Soft Co.,Ltd")> _
    Public Property mFedSort() As String
        Get
            Return tFedSort
        End Get
        Set(ByVal value As String)
            tFedSort = value
        End Set
    End Property

    '<Description(""), Category("Ada Soft Co.,Ltd")> _
    Public Property mCaptionCol() As String
        Get
            Return tCaptionCol
        End Get
        Set(ByVal value As String)
            tCaptionCol = value
        End Set
    End Property

    Public Property mFormName() As String
        Get
            Return tW_FormName
        End Get
        Set(ByVal value As String)
            tW_FormName = value
        End Set
    End Property

#End Region

#Region " UTILITY FUNCTION "
    Private Sub W_DATxInitialForm(ByVal ptSql As String)
        Dim oDbCon As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
        Try
            If oDbCon.State = ConnectionState.Closed Then oDbCon.Open()
            Me.oW_Cmd.CommandText = ptSql
            Me.oW_Cmd.CommandType = CommandType.Text
            Me.oW_Cmd.Connection = oDbCon
            Me.oW_Cmd.CommandTimeout = 30
            Me.oW_Adp.SelectCommand = Me.oW_Cmd
            oW_DsFilter = New DataSet
            oW_DtFilter = New DataTable
            Me.oW_Adp.Fill(Me.oW_DsFilter, Me.mTableName)
            Me.oW_DtFilter = Me.oW_DsFilter.Tables(Me.mTableName)

            With oW_DvFilter
                .Table = Me.oW_DsFilter.Tables(Me.mTableName)
                .AllowDelete = False
                .AllowEdit = False
                .AllowNew = False
            End With

            Me.Text = Me.mCaptionName
            Dim tColName As String
            Dim nCount As Integer

            Dim nRow As Integer = ogdFilter.Rows.Count - 1
            Dim nCols As Integer = ogdFilter.Cols.Count - 1

            Me.ogdFilter.DataSource = Me.oW_DvFilter

            '---------------------------------------------------------------------------------------
            tColName = Me.mColumnName
            Dim tArray() As String
            Dim tColTemp As String
            Dim nIndex As Integer
            tColTemp = Replace(tColName, ";", "|")
            tArray = tColTemp.Split("|")
            nIndex = 0
            For Each tCol As String In tArray
                If tCol <> "|" Then
                    nIndex += 1
                End If
            Next

            For nCol As Integer = 1 To nIndex
                ogdFilter.Cols(nCol).Caption = cCNSP.SP_STRtToken(tColName, ";", nCol, True)
            Next

            '-------------------------------------------------------------------------------------------- SA 7/12/2006
            Dim tAlign As String
            nCount = 0
            tAlign = Me.mAlignMent
            Dim tArrayAlign() As String
            Dim tAlignTemp As String
            tAlignTemp = Replace(tAlign, ";", "|")
            tArrayAlign = tAlignTemp.Split("|")
            nIndex = 0
            For Each tCol As String In tArrayAlign
                nIndex += 1
                Select Case tCol.ToUpper
                    Case "L"
                        Me.ogdFilter.Cols(nIndex).TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
                        Me.ogdFilter.Cols(nIndex).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
                    Case "R"
                        Me.ogdFilter.Cols(nIndex).TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter
                        Me.ogdFilter.Cols(nIndex).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter
                    Case "C"
                        Me.ogdFilter.Cols(nIndex).TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
                        Me.ogdFilter.Cols(nIndex).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
                    Case Else
                        Me.ogdFilter.Cols(nIndex).TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.GeneralCenter
                        Me.ogdFilter.Cols(nIndex).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.GeneralCenter
                End Select
            Next
            '--------------------------------------------------------------------------------------------------------------

            Dim tSort As String
            tSort = Me.mFedSort

            If tSort <> "" Then
                Dim tArraySort() As String
                tSort = Replace(tSort, ";", "|")
                tArraySort = tSort.Split("|")
                nIndex = 0
                For Each tCol As String In tArraySort
                    nIndex += 1
                    Select Case tCol
                        Case "1"
                            Me.ogdFilter.Cols(nIndex).AllowSorting = True
                        Case "0"
                            Me.ogdFilter.Cols(nIndex).AllowSorting = False
                    End Select
                Next
            End If


            'Ake edit 10/01/2012
            '�������ҧ�ͧ 1 Collumn
            Dim nColGridWidth As Integer
            If tArray.Length > 1 Then
                nColGridWidth = 140
            Else
                'nColGridWidth = 260
                nColGridWidth = 260
            End If

            '��Ҵ�ͧ Grid
            Dim nGridSize As Integer
            nGridSize = ((Me.ogdFilter.Cols.Count - 1) * nColGridWidth)
            Me.ogdFilter.Width = nGridSize

            '��Ҵ wBrowsers
            Dim a = Me.ogdFilter.Width
            Me.Width = a + 57

            '��Ѻ��Ҵ�ͧ Grid
            Dim nGridReSize As Integer
            nGridReSize = a + 29
            Me.ogdFilter.Width = nGridReSize

            '��Ѻ��Ҵ�ͧ Group box ����բ�Ҵ��ҡѺ Grid
            Me.ogbFilter.Width = nGridReSize

            '૵�������ҧ�ͧ Collumn
            For nCount = 1 To ogdFilter.Cols.Count - 1
                Me.ogdFilter.Cols(nCount).Width = nColGridWidth
            Next

            Me.W_DATxINI_Filter()

            W_DATxDefFilterCol()
        Catch ex As Exception
            cCNSP.SP_MSGnShowing(ex.Message, cCNEN.eEN_MSGStyle.nEN_MSGErr)
        Finally
            If oDbCon.State = ConnectionState.Open Then oDbCon.Close()
            oDbCon = Nothing
        End Try
    End Sub

    Private Sub W_DATxINI_Filter()
        Dim i As Integer, ix As Integer
        ix = 0
        For i = 0 To Me.ogdFilter.Cols.Count - 1
            Select Case i
                Case 0
                    Me.otbFilter00.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter00.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter00.Visible = False

                    Me.olaFilter00.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter00.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter00.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter00.Visible = False

                    ix = Me.otbFilter00.Location.X
                Case 1
                    ix = ix + Me.otbFilter00.Width
                    Me.otbFilter01.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter01.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter01.Visible = True

                    Me.olaFilter01.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter01.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter01.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter01.Visible = True
                Case 2
                    ix = ix + Me.otbFilter01.Width
                    Me.otbFilter02.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter02.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter02.Visible = True

                    Me.olaFilter02.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter02.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter02.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter02.Visible = True
                Case 3
                    ix = ix + Me.otbFilter02.Width
                    Me.otbFilter03.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter03.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter03.Visible = True

                    Me.olaFilter03.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter03.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter03.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter03.Visible = True
                Case 4
                    ix = ix + Me.otbFilter03.Width
                    Me.otbFilter04.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter04.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter04.Visible = True

                    Me.olaFilter04.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter04.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter04.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter04.Visible = True
                Case 5
                    ix = ix + Me.otbFilter04.Width
                    Me.otbFilter05.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter05.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter05.Visible = True

                    Me.olaFilter05.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter05.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter05.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter05.Visible = True
                Case 6
                    ix = ix + Me.otbFilter05.Width
                    Me.otbFilter06.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter06.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter06.Visible = True

                    Me.olaFilter06.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter06.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter06.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter06.Visible = True
                Case 7
                    ix = ix + Me.otbFilter06.Width
                    Me.otbFilter07.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter07.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter07.Visible = True

                    Me.olaFilter07.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter07.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter07.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter07.Visible = True
                Case 8
                    ix = ix + Me.otbFilter07.Width
                    Me.otbFilter08.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter08.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter08.Visible = True

                    Me.olaFilter08.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter08.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter08.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter08.Visible = True
                Case 9
                    ix = ix + Me.otbFilter08.Width
                    Me.otbFilter09.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter09.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter09.Visible = True

                    Me.olaFilter09.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter09.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter09.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter09.Visible = True
                Case 10
                    ix = ix + Me.otbFilter09.Width
                    Me.otbFilter10.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter10.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter10.Visible = True

                    Me.olaFilter10.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter10.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter10.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter10.Visible = True
                Case 11
                    ix = ix + Me.otbFilter10.Width
                    Me.otbFilter11.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter11.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter11.Visible = True

                    Me.olaFilter11.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter11.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter11.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter11.Visible = True
                Case 12
                    ix = ix + Me.otbFilter11.Width
                    Me.otbFilter12.Width = Me.ogdFilter.Cols(i).Width
                    Me.otbFilter12.Left = Me.ogdFilter.Cols(i).Left
                    Me.otbFilter12.Visible = True

                    Me.olaFilter12.Width = Me.ogdFilter.Cols(i).Width
                    Me.olaFilter12.Left = Me.ogdFilter.Cols(i).Left
                    Me.olaFilter12.Text = Me.ogdFilter.Cols(i).Caption
                    Me.olaFilter12.Visible = True
                Case Else
            End Select
        Next
    End Sub

    Private Sub W_DATxDefFilterCol()
        Select Case Me.nDefaultColumn
            '     Case 0
            '         Me.txtFilter00.Focus()
            Case 1
                Me.otbFilter01.Focus()
            Case 2
                Me.otbFilter02.Focus()
            Case 3
                Me.otbFilter03.Focus()
            Case 4
                Me.otbFilter04.Focus()
            Case 5
                Me.otbFilter05.Focus()
            Case 6
                Me.otbFilter06.Focus()
            Case 7
                Me.otbFilter07.Focus()
            Case 8
                Me.otbFilter08.Focus()
            Case 9
                Me.otbFilter09.Focus()
            Case 10
                Me.otbFilter10.Focus()
            Case 11
                Me.otbFilter11.Focus()
            Case 12
                Me.otbFilter12.Focus()
            Case Else
                Me.otbFilter01.Focus()
        End Select
    End Sub

    Public Sub W_DATxFilterDb()
        Dim i As Integer, tColName As String, tFilter As String
        Dim bStarted As Boolean
        Dim tTypeFilter As String
        bStarted = False
        tFilter = ""
        'oW_DvFilter.RowFilter.Remove(0, oW_DvFilter.Count - 1)

        For i = 0 To Me.oW_DvFilter.Table.Columns.Count - 1
            tColName = Me.oW_DvFilter.Table.Columns(i).ColumnName.ToString



            tTypeFilter = Mid(tColName, 2, 1)
            Select Case i
                Case 0
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tColName 'tFilter + tColName
                            tFilter &= " Like '%" + Trim(Me.otbFilter01.Text) + "%' "
                        Case "N", "C"
                            If otbFilter01.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter01.Text & "') ='" & Me.otbFilter01.Text & "'"
                            End If
                    End Select

                Case 1
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter02.Text) + "%' "
                        Case "N", "C"
                            If otbFilter02.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter02.Text & "') ='" & Me.otbFilter02.Text & "'"
                            End If
                    End Select

                Case 2
                    'Run 51-03-03
                    If Me.otbFilter03.Text.Trim = "" Then Exit Select
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter03.Text) + "%' "
                        Case "N", "C"
                            If otbFilter03.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter03.Text & "') ='" & Me.otbFilter03.Text & "'"
                            End If
                            '   dv.RowFilter = "IsNull(Departure_Date, '01/01/1900') = '01/01/1900'"
                    End Select

                Case 3
                    'Run 51-03-03
                    If Me.otbFilter04.Text.Trim = "" Then Exit Select
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter04.Text) + "%' "
                        Case "N", "C", "D"
                            If otbFilter04.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter04.Text & "') ='" & Me.otbFilter04.Text & "'"
                            End If
                    End Select

                Case 4
                    'Run 51-03-03
                    If Me.otbFilter05.Text.Trim = "" Then Exit Select
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter05.Text) + "%' "
                        Case "N", "C"
                            If otbFilter05.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter05.Text & "') ='" & Me.otbFilter05.Text & "'"
                            End If
                    End Select

                Case 5
                    'Run 51-03-03
                    If Me.otbFilter06.Text.Trim = "" Then Exit Select
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter06.Text) + "%' "
                        Case "N", "C"
                            If otbFilter06.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter06.Text & "') ='" & Me.otbFilter06.Text & "'"
                            End If
                    End Select

                Case 6
                    'Run 51-03-03
                    If Me.otbFilter07.Text.Trim = "" Then Exit Select
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter07.Text) + "%' "
                        Case "N", "C"
                            If otbFilter07.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter07.Text & "') ='" & Me.otbFilter07.Text & "'"
                            End If
                    End Select

                Case 7
                    'Run 51-03-03
                    If Me.otbFilter08.Text.Trim = "" Then Exit Select
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter08.Text) + "%' "
                        Case "N", "C"
                            If otbFilter04.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter04.Text & "') ='" & Me.otbFilter04.Text & "'"
                            End If
                    End Select

                Case 8
                    'Run 51-03-03
                    If Me.otbFilter09.Text.Trim = "" Then Exit Select
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter09.Text) + "%' "
                        Case "N", "C"
                            If otbFilter09.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter09.Text & "') ='" & Me.otbFilter09.Text & "'"
                            End If
                    End Select

                Case 9
                    'Run 51-03-03
                    If Me.otbFilter10.Text.Trim = "" Then Exit Select
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter10.Text) + "%' "
                        Case "N", "C"
                            If otbFilter10.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter10.Text & "') ='" & Me.otbFilter10.Text & "'"
                            End If
                    End Select

                Case 10
                    'Run 51-03-03
                    If Me.otbFilter11.Text.Trim = "" Then Exit Select
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter11.Text) + "%' "
                        Case "N", "C"
                            If otbFilter11.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter11.Text & "') ='" & Me.otbFilter11.Text & "'"
                            End If
                    End Select

                Case 11
                    'Run 51-03-03
                    If Me.otbFilter12.Text.Trim = "" Then Exit Select
                    Select Case tTypeFilter
                        Case "T"
                            tFilter = tFilter + "And " + tColName + " Like '%" + Trim(Me.otbFilter12.Text) + "%' "
                        Case "N", "C"
                            If otbFilter12.Text <> "" Then
                                If tFilter <> "" Then tFilter &= "AND "
                                tFilter &= "IsNull(" & tColName & ", '" & Me.otbFilter12.Text & "') ='" & Me.otbFilter12.Text & "'"
                            End If
                    End Select

                Case Else

            End Select
        Next

        Me.oW_DvFilter.RowFilter = tFilter

        '========================Ake Mark : 16/05/2012======================== '����ռ����áѺ grid
        'Dim nGridWidth As Integer, nCount As Integer
        'Dim nDiff As Integer
        'nGridWidth = 20
        'For nCount = 0 To Me.ogdFilter.Cols.Count - 1
        '    nGridWidth += Me.ogdFilter.Cols(nCount).Width
        'Next
        'If nGridWidth < 400 Then
        '    nDiff = 400 - nGridWidth
        '    nGridWidth = 400
        '    Me.ogdFilter.Cols(nCount - 1).Width = Me.ogdFilter.Cols(nCount - 1).Width + nDiff
        'End If
    End Sub

    Private Sub W_SETxSetTextControl()
        '===========================
        'Cmt : Set Text Control
        '===========================
        Me.ogbFilter.Text = Split(cCNCS.tCS_CNFilter, ";")(cCNVB.nVB_CutLng - 1)
        Me.ocmSelect.Text = Split(cCNCS.tCS_CNOk, ";")(cCNVB.nVB_CutLng - 1)
        Me.ocmExit.Text = Split(cCNCS.tCS_Exit, ";")(cCNVB.nVB_CutLng - 1)
    End Sub


#End Region

#Region " EVENT "

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmExit.Click
        Me.Close()
    End Sub

    Private Sub frmCSBrowser_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        W_DATxDefFilterCol()
    End Sub

    Private Sub frmCSBrowser_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then
            SendKeys.Send("{TAB}")
        End If
        If e.Alt = True And e.KeyCode = Keys.S Then
            Me.W_DATxSelectItem()
        End If
        If e.Alt = True And e.KeyCode = Keys.X Then
            Me.Close()
        End If
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub frmCSBrowser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.W_SETxSetTextControl()
        Dim tSql As String = mCommandText
        W_DATxInitialForm(tSql)

        'Me.SetBounds(VB6.TwipsToPixelsX((VB6.PixelsToTwipsX(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width) - VB6.PixelsToTwipsX(Width)) \ 2), VB6.TwipsToPixelsY((VB6.PixelsToTwipsY(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) - VB6.PixelsToTwipsY(Height)) \ 2), 0, 0, Windows.Forms.BoundsSpecified.X Or Windows.Forms.BoundsSpecified.Y)
    End Sub

#Region "Text Box Key Down"
    Private Sub otbFilter00_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter00.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter00.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter01.Text, Microsoft.VisualBasic.Right(otbFilter00.Text, 1))
        End If
    End Sub

    Private Sub otbFilter01_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter01.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter01.Text.Trim = "" Then Exit Sub
            ogdFilter.Refresh()
            W_SQLxData(otbFilter01.Text, Microsoft.VisualBasic.Right(otbFilter01.Name, 1))
        End If
    End Sub

    Private Sub otbFilter02_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter02.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter02.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter02.Text, Microsoft.VisualBasic.Right(otbFilter02.Name, 1))
        End If
    End Sub

    Private Sub otbFilter03_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter03.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter03.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter03.Text, Microsoft.VisualBasic.Right(otbFilter03.Name, 1))
        End If
    End Sub

    Private Sub otbFilter04_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter04.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter04.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter04.Text, Microsoft.VisualBasic.Right(otbFilter04.Name, 1))
        End If
    End Sub

    Private Sub otbFilter05_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter05.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter05.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter05.Text, Microsoft.VisualBasic.Right(otbFilter05.Name, 1))
        End If
    End Sub

    Private Sub otbFilter06_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter06.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter06.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter06.Text, Microsoft.VisualBasic.Right(otbFilter06.Name, 1))
        End If
    End Sub

    Private Sub otbFilter07_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter07.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter07.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter07.Text, Microsoft.VisualBasic.Right(otbFilter07.Name, 1))
        End If
    End Sub

    Private Sub otbFilter08_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter08.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter08.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter08.Text, Microsoft.VisualBasic.Right(otbFilter08.Text, 1))
        End If
    End Sub

    Private Sub otbFilter09_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter09.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter09.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter09.Text, Microsoft.VisualBasic.Right(otbFilter09.Text, 1))
        End If
    End Sub

    Private Sub otbFilter10_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter10.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter10.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter10.Text, Microsoft.VisualBasic.Right(otbFilter10.Text, 1))
        End If
    End Sub

    Private Sub otbFilter11_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles otbFilter11.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter11.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter11.Text, Microsoft.VisualBasic.Right(otbFilter11.Text, 1))
        End If
    End Sub

    Private Sub otbFilter12_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles olaFilter12.KeyDown
        If e.KeyData = Keys.Enter Then
            If otbFilter12.Text.Trim = "" Then Exit Sub
            W_SQLxData(otbFilter12.Text, Microsoft.VisualBasic.Right(otbFilter12.Text, 1))
        End If
    End Sub

    'Private Function W_CHKbDataInGrid() As Boolean 'Ake Add new 14/05/2012
    '    W_CHKbDataInGrid = False '����բ������ Grid
    '    If ogdFilter.Rows.Count = ogdFilter.Rows.Fixed Then Exit Function
    '    W_CHKbDataInGrid = True '�բ������ Grid
    'End Function

    Private Sub W_SQLxData(ByVal ptCon As String, ByVal ptIdx As String)
        Dim tSql$
        Dim tTableName As String
        Dim tColName As String = ""
        Dim oDbDtb As New DataTable

        tTableName = oW_DvFilter.Table.ToString
        tColName &= Me.oW_DvFilter.Table.Columns(0).ColumnName.ToString
        For i = 1 To Me.oW_DvFilter.Table.Columns.Count - 1
            tColName &= "," & Me.oW_DvFilter.Table.Columns(i).ColumnName.ToString
        Next
        tSql = ""
        tSql = "SELECT " & tColName
        tSql &= " FROM " & tTableName.Trim

        Dim tCon() As String = tCaptionCol.Split(",")

        tSql &= " WHERE " & tCon(CInt(ptIdx) - 1) & " Like '%" & ptCon & "%'"

        If tW_FormName = "wCrdValManage" Then
            Dim tField() As String = tColName.Split(",")
            tSql &= " AND FTPosType = '4' " '*[Anubis][RQ1303-040][2013-04-02] - When Enter At Control otbFilter00 Will Error <::> Edit String In Variable tSql
        End If
        '10/08/2012 MARK
        'If tVB_FormName.Trim = "wCrdValManage" Then 
        '    tSql &= " AND FTPosType = '4' "
        'End If
        W_DATxInitialForm(tSql)
    End Sub

#End Region

    Private Sub otbFilter00_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles otbFilter00.KeyPress,
    otbFilter01.KeyPress, otbFilter02.KeyPress, otbFilter03.KeyPress, otbFilter04.KeyPress, otbFilter05.KeyPress, otbFilter06.KeyPress, otbFilter07.KeyPress,
    otbFilter08.KeyPress, otbFilter09.KeyPress, otbFilter10.KeyPress, otbFilter11.KeyPress, otbFilter12.KeyPress
        e.Handled = cCNSP.SP_CNKBbChkKeyAscii(Asc(e.KeyChar))
    End Sub

    Private Sub txtFilter00_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter00.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter01_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter01.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter02_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter02.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter03_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter03.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter04_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter04.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter05_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter05.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter06_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter06.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter07_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter07.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter08_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter08.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter09_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter09.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter10.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter11_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter11.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub txtFilter12_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otbFilter12.TextChanged
        W_DATxFilterDb()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmSelect.Click
        W_DATxSelectItem()
    End Sub

    Private Sub W_DATxSelectItem()
        Dim nRow As Integer, nCol As Integer
        Dim i As Integer

        nRow = Me.ogdFilter.RowSel
        nCol = Me.ogdFilter.ColSel
        If nRow < 0 Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN703, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Me.bReturn = False
            Exit Sub
        End If

        Me.oW_DrReturn = Me.oW_DtFilter.NewRow
        For i = 1 To Me.ogdFilter.Cols.Count - 1
            oW_DrReturn(i - 1) = Me.ogdFilter.GetData(nRow, i)
        Next

        Me.bReturn = True
        Me.Close()

    End Sub

    Private Sub dgdFilter_AfterResizeColumn(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles ogdFilter.AfterResizeColumn
        Me.W_DATxINI_Filter()
    End Sub

    Private Sub dgdFilter_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ogdFilter.DoubleClick
        Me.W_DATxSelectItem()
    End Sub

    Private Sub frmCSBrowser_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Dim nDiff As Integer, nCount As Integer
        Dim nWidthOld As Integer, nWidthNew As Integer

        Me.ocmExit.Top = Me.Height - Me.ocmExit.Height - 45
        Me.ocmExit.Left = Me.Width - 120
        Me.ocmSelect.Top = Me.ocmExit.Top
        Me.ocmSelect.Left = Me.ocmExit.Left - Me.ocmSelect.Width - 5

        Me.ogbFilter.Top = Me.Height - Me.ogbFilter.Height - 103
        Me.ogbFilter.Width = Me.Width - 20
        Me.ogdFilter.Height = Me.ogbFilter.Top - 20
        nWidthOld = Me.ogdFilter.Width
        Me.ogdFilter.Width = Me.ogbFilter.Width
        nWidthNew = Me.ogdFilter.Width
        nDiff = (nWidthNew - nWidthOld)
        nCount = Me.ogdFilter.Cols.Count
        Me.ogdFilter.Cols(nCount - 1).Width = Me.ogdFilter.Cols(nCount - 1).Width + nDiff
        Me.ocmKeyboard.Top = Me.ocmExit.Top
        Me.ocmKeyboard.Left = Me.ogbFilter.Left
        Me.W_DATxINI_Filter()
    End Sub

    Private Sub ocmKeyboard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmKeyboard.Click
        cCNSP.SP_ProcessKeyboard() 'Call
    End Sub

#End Region

End Class