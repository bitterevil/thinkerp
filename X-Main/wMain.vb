Option Explicit On
Imports System.IO
Imports System.Data.SqlClient

Public Class wMain

#Region "event"

#Region "button"
    Private Sub omnFileExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles omnFileExit.Click, otoExit.Click
        Me.Close()
    End Sub

    Private Sub otoChgPwd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otoChgPwd.Click, omnFileChgPwd.Click
        cLog.C_CALxWriteLog("wChangerPass")
        wChangePass.ShowDialog()
    End Sub

    Private Sub otoHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otoHelp.Click, omnHelpHowTo.Click
        Dim newProc As Diagnostics.Process
        Dim tAppPath As String

        tAppPath = Application.StartupPath
        tAppPath &= "\AdaLinkTHSHelp.chm"

        If System.IO.File.Exists(tAppPath) = False Then
            cCNSP.SP_MSGnShowing(cCNMS.tMS_CN006 & ";" & tAppPath, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            Exit Sub
        End If
        newProc = Diagnostics.Process.Start(tAppPath)
    End Sub

#End Region

#Region "form"

    Private Sub wMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            If cCNSP.SP_MSGnShowing(cCNMS.tMS_CN003, cCNEN.eEN_MSGStyle.nEN_MSGYesNo) = MsgBoxResult.No Then
                cLog.C_CALxWriteLog("Exit")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub wMain_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub wMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set Culture
        Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-GB")

        Dim oDatabase As New cDatabaseLocal

        'Default Eng
        cCNVB.nVB_CutLng = 2
        cCNSP.SP_LNGxSetDefAftChg()

        Me.Text &= " - [" & My.Application.Info.Version.ToString & "]"

RESTART:

        Try
            '���� Get file config
            AdaConfig.cConfig.C_GETxConfigXml()
            '���� Get file config API
            AdaConfig.cConfig.C_GETxConfigXmlAPI()

            'Get ��ҷ����ҡ Config ���ŧ control 
            Me.ostMInfo.Text = AdaConfig.cConfig.AdaConfigXml
            Me.ostMDB.Text = AdaConfig.cConfig.oConnSource.tServer & "/" & AdaConfig.cConfig.oConnSource.tCatalog

            'Check Connection
            If AdaConfig.cConfig.C_CHKbSQLSourceConnect = True Then
                If cApp.C_CALaCheckTableLog.Count > 0 Then
                    '�Դ�������� �ʴ� Msg & �ʴ�˹�Ҩ� wSetting �ҡ��� restart
                    cCNSP.SP_MSGnShowing(cCNMS.tMS_CN110, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
                    Dim oFrmSetting As New wSetting()
                    If oFrmSetting.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        oFrmSetting.Dispose()
                        Application.Restart()
                    Else
                        End
                    End If

                End If

            Else
                '�Դ�������� �ʴ� Msg & �ʴ�˹�Ҩ� wSetting �ҡ��� restart
                cCNSP.SP_MSGnShowing(cCNMS.tMS_CN004, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
                Dim oFrmSetting As New wSetting()
                If oFrmSetting.ShowDialog() = Windows.Forms.DialogResult.Yes Then
                    oFrmSetting.Dispose()
                    'Application.Restart()
                    GoTo RESTART
                Else
                    End
                End If
            End If

            While True
                Dim oLogin As New wLogin

                If oLogin.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    cCNVB.nVB_CutLng = AdaConfig.cConfig.oApplication.nLanguage
                    cCNSP.SP_LNGxSetDefAftChg()
                    cCNSP.SP_FrmSetCapControl(Me)
                    cCNSP.SP_GETxVariable()

                    'Create Table Log
                    oDatabase.C_CALnExecuteNonQuery(cApp.tSQLCmdCheckTableTemp)

                    'Update Mapping '*CH 01-04-2015
                    '   oDatabase.C_CALnExecuteNonQuery(cApp.tSQLCmdUpdMapping)
                    Exit While
                Else
                    End
                End If
            End While

        Catch ex As Exception
            cCNSP.SP_MSGShowError(ex.Message)
            End
        End Try

        cLog.C_CALxWriteLog("wMain")
    End Sub

    Private Sub otoExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otoExport.Click, omnExport.Click
        cLog.C_CALxWriteLog("wExports")
        wExports.ShowDialog()
        wExports.Dispose()
        'wExports2.ShowDialog()
        'wExports2.Dispose()
    End Sub

    Private Sub otoUpload_Click(sender As Object, e As EventArgs) Handles otoUpload.Click
        cLog.C_CALxWriteLog("wUploadPmt")
        '  wUploadPmt.ShowDialog()
        ' wUploadPmt.Dispose()
    End Sub

    Private Sub otmForm_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles otmForm.Tick
        Me.ostDate.Text = ": " & Format(Date.Now, "dd/MM/yyyy")
        Me.ostTime.Text = ": " & Format(Date.Now, "HH:mm:ss")
    End Sub

    Private Sub C_FRMxOpenExpReconcile(sender As Object, e As EventArgs)
        cLog.C_CALxWriteLog("wExpReconcile")
        'wExpReconcile.ShowDialog()
        '  wExpReconcile.Dispose()
    End Sub

    Private Sub C_FRMxOpenExpExpense(sender As Object, e As EventArgs)
        cLog.C_CALxWriteLog("wExpExpense")
        ' wExpExpense.ShowDialog()
        'wExpExpense.Dispose()
    End Sub

    Private Sub C_FRMxOpenExpCashTnf(sender As Object, e As EventArgs)
        cLog.C_CALxWriteLog("wExpCashTnf")
        'wExpCashTnf.ShowDialog()
        ' wExpCashTnf.Dispose()
    End Sub
#End Region

#End Region

    Private Sub otoChangeLang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otoChangeLang.Click
        '����¹����
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then
            cCNVB.nVB_CutLng = 1
            Me.otoChangeLang.Image = Me.oimForm.Images(1)
        Else
            Me.otoChangeLang.Image = Me.oimForm.Images(0)
            cCNVB.nVB_CutLng = 2
        End If
        AdaConfig.cConfig.oApplication.nLanguage = cCNVB.nVB_CutLng
        AdaConfig.cConfig.C_SETbUpdateLang()
        cCNSP.SP_LNGxSetDefAftChg()
        cCNSP.SP_FrmSetCapControl(Me)
        cLog.C_CALxWriteLog("Change Language " & cCNVB.nVB_CutLng)
    End Sub

    Private Sub otoLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otoLog.Click, omnToolsLog.Click
        cLog.C_CALxWriteLog("wLog")
        wHisLog.ShowDialog()
        wHisLog.Dispose()
        'wLog.ShowDialog()
        'wLog.Dispose()
    End Sub

    Private Sub omnHelpAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles omnHelpAbout.Click
        cLog.C_CALxWriteLog("wAbout")
        wAbout.ShowDialog()
        wAbout.Dispose()
    End Sub

    Private Sub otoImport_Click(sender As System.Object, e As System.EventArgs) Handles otoImport.Click
        cLog.C_CALxWriteLog("wImports")
        'wImports.ShowDialog()
        ' wImports.Dispose()
    End Sub

    Private Sub omnToolsOption_Click(sender As System.Object, e As System.EventArgs) Handles omnToolsOption.Click
        Me.Cursor = Cursors.WaitCursor
        Dim oFrmSetting As New wSetting()
        Me.Cursor = Cursors.Default
        cLog.C_CALxWriteLog("wSetting")
        oFrmSetting.ShowDialog()
        If oFrmSetting.DialogResult = Windows.Forms.DialogResult.Yes Then
0:          Application.Restart()
        End If
    End Sub

    Private Sub omnToolsRebuild_Click(sender As System.Object, e As System.EventArgs)
        cLog.C_CALxWriteLog("wIndex")
        wIndex.ShowDialog()
        wIndex.Dispose()
    End Sub


End Class
