﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Public Class wSetting

    Property nW_IndexMenu As Integer = 0
    Property bW_Lock As Boolean = False

#Region "Initail"
    Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        'เรียกหน้า User Control ใส่ไว้ในตัวแปร  oW_UsrCtr
        Me.oW_UsrCtr = New cControls()
    End Sub
#End Region

#Region "Variable"

    Private oW_UsrCtr As cControls
    Private bW_Close As Boolean = False

#End Region

#Region "Sub And Function"

    Private Sub olvMenu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles olvMenu.SelectedIndexChanged
        For Each oItem As ListViewItem In Me.olvMenu.Items
            oItem.ForeColor = Color.Black
            oItem.BackColor = Color.White
            If oItem.Selected = True Then
                Dim nObj = CType(oItem.Index + 1, cControls.eType)
                Me.Cursor = Cursors.WaitCursor
                Me.oW_UsrCtr.W_GETxCurCtl(Me.opnForm, nObj)
                Me.Cursor = Cursors.Default
                Me.olaDesForm.Text = Me.oW_UsrCtr.W_GETtCurDes
                oItem.ForeColor = Color.White
                oItem.BackColor = Color.RoyalBlue
                Exit Sub
            End If
        Next
    End Sub

    Private Sub wSettingExport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Me.bW_Close = True Then
            Me.bW_Close = False
            e.Cancel = True
        End If
    End Sub

    Private Sub wCreateTask_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Me.bW_Lock = True Then
            Me.olvMenu.Enabled = False
        Else
            Me.olvMenu.Enabled = True
        End If

        cCNSP.SP_FrmSetCapControl(Me)
        If Me.olvMenu.Items.Count > 0 Then Me.olvMenu.Items(nW_IndexMenu).Selected = True

    End Sub
    Private Sub ocmSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmSave.Click

        If cCNSP.SP_MSGnShowing(cCNMS.tMS_CN108, cCNEN.eEN_MSGStyle.nEN_MSGYesNo) = MsgBoxResult.Yes Then

            'ส่ง ค่า จาก Usercontrol ที่เก็บในตัวแปร oW_UsrCtr  ไป connectbase ว่าสามารถ connect ได้ไหม 

            If oW_UsrCtr.W_DATbChkDb = True Then
                If oW_UsrCtr.W_DATbConnChage = True Then
                    Me.DialogResult = Windows.Forms.DialogResult.Yes
                Else
                    Me.DialogResult = Windows.Forms.DialogResult.No
                End If

                'Member Database *CH 18-10-2014
                If oW_UsrCtr.W_DATbMemConnChage = True Then
                    Me.DialogResult = Windows.Forms.DialogResult.Yes
                Else
                    Me.DialogResult = Windows.Forms.DialogResult.No
                End If

                'สร้าง/เขียนลง Log การกระทำ
                cLog.C_CALxWriteLog("wSetting > Save")
                'Save Config to Xml File
                'เช็คว่า โฟลเดอข้างบน มีอยู่จริงทั้งหมดไหม ถ้ามี ให้ save xml file
                '  If bInbox = True And bBackUp = True And bLog = True And bOutBox = True Then 'Check save (True All = Save)  ***PAN 2016-05-16
                oW_UsrCtr.W_DATxSaveConfig()


                'Reload Config
                AdaConfig.cConfig.C_SETbUpdateXml()
                AdaConfig.cConfig.C_SETbUpdateXmlAPI()
                W_GENxCrtMapping()

                Me.bW_Close = False
                Me.Close()
                Exit Sub
            Else
                cCNSP.SP_MSGnShowing(cCNMS.tMS_CN104, cCNEN.eEN_MSGStyle.nEN_MSGWarn)
            End If
        End If

        Me.bW_Close = True

    End Sub

    ''' <summary>
    ''' สร้าง Table Mapping Branch 
    ''' </summary>
    Private Sub W_GENxCrtMapping()
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oSQL As New StringBuilder
            oSQL.Clear()
            oSQL.AppendLine(" IF NOT EXISTS (SELECT * FROM sys.objects")
            oSQL.AppendLine(" WHERE object_id = OBJECT_ID(N'TLNKMappingBranch') AND type in (N'U'))")
            oSQL.AppendLine(" BEGIN ")
            oSQL.AppendLine(" CREATE TABLE [dbo].[TLNKMappingBranch] ")
            oSQL.AppendLine(" ([FTBchCode] [VARCHAR](50) NULL,")
            oSQL.AppendLine(" [FTBchName] [VARCHAR](150) NULL,")
            oSQL.AppendLine(" [FTDefaultValue] [VARCHAR](20) NULL,")
            oSQL.AppendLine(" [FTUrsValue] [VARCHAR](20) NULL) ")
            oSQL.AppendLine(" INSERT [dbo].[TLNKMappingBranch] ")
            oSQL.AppendLine(" ([FTBchCode], ")
            oSQL.AppendLine(" [FTBchName],")
            oSQL.AppendLine(" [FTDefaultValue],")
            oSQL.AppendLine(" [FTUrsValue]) ")
            oSQL.AppendLine(" SELECT FTBchCode, FTBchName, ")
            oSQL.AppendLine(" (CASE  WHEN FTBchCode = '001'  THEN '01' ")
            oSQL.AppendLine(" WHEN FTBchCode = '002' THEN '03' ")
            oSQL.AppendLine(" WHEN FTBchCode = '003' THEN '04' ")
            oSQL.AppendLine(" WHEN FTBchCode = '004' THEN '02'")
            oSQL.AppendLine(" ELSE '01' END) AS FTDefaultValue ,")
            oSQL.AppendLine(" (CASE  WHEN FTBchCode = '001'  THEN '01' ")
            oSQL.AppendLine(" WHEN FTBchCode = '002' THEN '03' ")
            oSQL.AppendLine(" WHEN FTBchCode = '003' THEN '04' ")
            oSQL.AppendLine(" WHEN FTBchCode = '004' THEN '02' ")
            oSQL.AppendLine(" ELSE '01' END) AS FTUrsValue ")
            oSQL.AppendLine(" FROM TCNMBranch")
            oSQL.AppendLine(" END ")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)
            W_GENxCrtPayMapping()

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Create Table การชำระเงิน
    ''' </summary>
    Public Sub W_GENxCrtPayMapping()
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oSQL As New StringBuilder
            oSQL.Clear()
            oSQL.AppendLine(" IF NOT EXISTS (SELECT * FROM sys.objects ")
            oSQL.AppendLine(" WHERE object_id = OBJECT_ID(N'TLNKMappingPayment') AND type in (N'U')) ")
            oSQL.AppendLine(" BEGIN")
            oSQL.AppendLine(" CREATE TABLE [dbo].[TLNKMappingPayment] ")
            oSQL.AppendLine(" ([FTRcvCode] [VARCHAR](50) NULL, ")
            oSQL.AppendLine(" [FTRcvName] [VARCHAR](150) NULL, ")
            oSQL.AppendLine(" [FTPayDefaultValue] [VARCHAR](20) NULL,")
            oSQL.AppendLine(" [FTPayUrsValue] [VARCHAR](20) NULL) ")
            oSQL.AppendLine(" INSERT [dbo].[TLNKMappingPayment] ")
            oSQL.AppendLine(" ([FTRcvCode],[FTRcvName],[FTPayDefaultValue],[FTPayUrsValue]) ")
            oSQL.AppendLine(" SELECT FTRcvCode, FTRcvName,")
            oSQL.AppendLine(" (CASE WHEN FTRcvCode = '001' THEN 'CASH' ")
            oSQL.AppendLine("  WHEN FTRcvCode = '002' THEN 'CREDIT' ")
            oSQL.AppendLine("  WHEN FTRcvCode = '005' THEN 'TRANSFER' ")
            oSQL.AppendLine("  ELSE '' End) As FTPayDefaultValue,")
            oSQL.AppendLine("  (CASE   WHEN FTRcvCode = '001' THEN 'CASH'")
            oSQL.AppendLine("  WHEN FTRcvCode = '002' THEN 'CREDIT'")
            oSQL.AppendLine("  WHEN FTRcvCode = '005' THEN 'TRANSFER'")
            oSQL.AppendLine("  ELSE '' End) As FTPayUrsValue FROM TPSMRcv")
            oSQL.AppendLine(" END ")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ocmClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ocmClose.Click
        Me.DialogResult = Windows.Forms.DialogResult.No
        Me.bW_Close = False
        'Me.Close()
    End Sub

#End Region

End Class