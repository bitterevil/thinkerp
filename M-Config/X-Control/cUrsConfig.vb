﻿Public Class cUrsConfig
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Public Property W_DATtUrl() As String
        Get
            Return otbUrl.Text.Trim
        End Get
        Set(value As String)
            Me.otbUrl.Text = value
        End Set
    End Property

    Public Property W_DATtMethod() As String
        Get
            Return otbMethod.Text.Trim
        End Get
        Set(value As String)
            Me.otbMethod.Text = "POST"
        End Set
    End Property

    Public Property W_DATtContent() As String
        Get
            Return otbCtype.Text.Trim
        End Get
        Set(value As String)
            Me.otbCtype.Text = "application/json"
        End Set
    End Property

    Public Property W_DATtAppKey() As String
        Get
            Return otbAppKey.Text.Trim
        End Get
        Set(value As String)
            Me.otbAppKey.Text = value
        End Set
    End Property

    Public Property W_DATtUrlReturn() As String
        Get
            Return otbUrlRe.Text.Trim
        End Get
        Set(value As String)
            Me.otbUrlRe.Text = value
        End Set
    End Property

    Public Property W_DATtUrlReturnAll() As String
        Get
            Return otbReAll.Text.Trim
        End Get
        Set(value As String)
            Me.otbReAll.Text = value
        End Set
    End Property

    Public Property W_DATtType() As String
        Get
            If orbReturnBill.Checked = True Then
                Return "0"
            ElseIf orbReturn.Checked = True Then
                Return "1"
            End If

        End Get
        Set(value As String)

            Me.otbReAll.Text = value
            If orbReturnBill.Checked = True Then
                Me.orbReturnBill.Checked = value
            ElseIf orbReturn.Checked = True Then
                orbReturn.Checked = value
            End If

        End Set
    End Property

    Private Sub cUrsConfig_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If cCNVB.nVB_CutLng = 1 Then
            olaTitleType.Text = "ประเภทการคืน"
            orbReturnBill.Text = "คืนทั้งบิล"
            orbReturn.Text = "คืนบางรายการ"
        ElseIf cCNVB.nVB_CutLng = 2 Then
            olaTitleType.Text = "Export Type"
            orbReturnBill.Text = "ReturnBill"
            orbReturn.Text = "Return"
        End If

        Dim tType As String = ""
        tType = AdaConfig.cConfig.oConfigAPI.tTypeReturn

        If tType = "0" Then
            orbReturnBill.Checked = True
        ElseIf tType = "1" Then
            orbReturn.Checked = True
        End If

    End Sub


End Class
