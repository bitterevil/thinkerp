﻿Imports System.Text
Imports C1.Win.C1FlexGrid

Public Class cUsrMappingBranch
    Private bW_Load As Boolean = False

    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub


    ''' <summary>
    ''' Setting Header grid.
    ''' </summary>
    Public Sub W_SETxInitGridBranch()
        With Me.ogdBranch
            .AllowResizing = AllowResizingEnum.None
            .AllowSorting = AllowSortingEnum.None
            .AllowDragging = AllowDraggingEnum.None
            .AllowEditing = True

            .Cols("FTBchCode").Caption = IIf(cCNVB.nVB_CutLng = 2, "BranchCode.", "รหัสสาขา")
            .Cols("FTBchCode").AllowEditing = False
            .Cols("FTBchCode").Width = 100

            .Cols("FTBchName").Caption = IIf(cCNVB.nVB_CutLng = 2, "Name", "ชื่อ")
            .Cols("FTBchName").AllowEditing = False
            .Cols("FTBchName").Width = 100

            .Cols("FTDefaultValue").Caption = IIf(cCNVB.nVB_CutLng = 2, "Default", "ค่าเริ่มต้น")
            .Cols("FTDefaultValue").AllowEditing = False
            .Cols("FTDefaultValue").Width = 100

            .Cols("FTUrsValue").Caption = IIf(cCNVB.nVB_CutLng = 2, "Value", "กำหนดเอง")
            .Cols("FTUrsValue").AllowEditing = True
            .Cols("FTUrsValue").Width = 112


        End With

        Me.bW_Load = True

    End Sub


    Public Sub W_SETxInitGridPayment()
        With Me.ogdPayType

            .AllowResizing = AllowResizingEnum.None
            .AllowSorting = AllowSortingEnum.None
            .AllowDragging = AllowDraggingEnum.None
            .AllowEditing = True

            .Cols("FTRcvCode").Caption = IIf(cCNVB.nVB_CutLng = 2, "RcvCode.", "รหัสการชำระเงิน")
            .Cols("FTRcvCode").AllowEditing = False
            .Cols("FTRcvCode").Width = 100


            .Cols("FTRcvName").Caption = IIf(cCNVB.nVB_CutLng = 2, "Desscription", "คำอธิบาย")
            .Cols("FTRcvName").AllowEditing = False
            .Cols("FTRcvName").Width = 100

            .Cols("FTPayDefaultValue").Caption = IIf(cCNVB.nVB_CutLng = 2, "Default", "ค่าเริ่มต้น")
            .Cols("FTPayDefaultValue").AllowEditing = False
            .Cols("FTPayDefaultValue").Width = 100

            .Cols("FTPayUrsValue").Caption = IIf(cCNVB.nVB_CutLng = 2, "Value", "กำหนดเอง")
            .Cols("FTPayUrsValue").AllowEditing = True
            .Cols("FTPayUrsValue").Width = 95

        End With
        Me.bW_Load = True

    End Sub


    Private Sub cUsrMappingBranch_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        W_SETxLoadBranch()
        W_SETxLoadPayment()

        If cCNVB.nVB_CutLng = 1 Then
            olaBranch.Text = "สาขา"
            olaPayType.Text = "ประเภทการชำระ"

        End If

    End Sub

    Public Sub W_SETxLoadBranch()
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oSql As New StringBuilder
            Dim oDbTbl As New DataTable

            oSql.Clear()
            oSql.AppendLine(" SELECT FTBchCode, FTBchName, FTDefaultValue, FTUrsValue ")
            oSql.AppendLine(" FROM TLNKMappingBranch")


            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString())
            Me.ogdBranch.DataSource = oDbTbl

            W_SETxInitGridBranch()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub W_SETxLoadPayment()
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oSql As New StringBuilder
            Dim oDbTbl As New DataTable

            oSql.Clear()
            oSql.AppendLine(" SELECT FTRcvCode, FTRcvName, FTPayDefaultValue, FTPayUrsValue ")
            oSql.AppendLine(" FROM  TLNKMappingPayment ")

            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString)
            Me.ogdPayType.DataSource = oDbTbl
            W_SETxInitGridPayment()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub ogdBranch_SetupEditor(sender As Object, e As RowColEventArgs) Handles ogdBranch.SetupEditor
        With ogdBranch
            If e.Col = .Cols("FTUrsValue").Index Then
                Dim oControlEdit As New TextBox
                .Cols("FTUrsValue").Editor = oControlEdit
            End If
        End With
    End Sub

    Private Sub ogdBranch_AfterEdit(sender As Object, e As RowColEventArgs) Handles ogdBranch.AfterEdit
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oSQL As New StringBuilder

            Dim oConGrid As C1FlexGrid = CType(sender, C1FlexGrid)
            Dim tGetUValue As String = ogdBranch(oConGrid.Row, oConGrid.Col)
            Dim tGetName As String = ogdBranch(oConGrid.Row, 1)
            oSQL.AppendLine("UPDATE TLNKMappingBranch SET FTUrsValue = '" & tGetUValue & "'")
            oSQL.AppendLine("WHERE FTBchCode ='" & tGetName & "' ")

            If oDatabase.C_CALnExecuteNonQuery(oSQL.ToString) = 1 Then
                MsgBox("ปรับปรุงข้อมูลเรียบร้อย")
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ogdPayType_SetupEditor(sender As Object, e As RowColEventArgs) Handles ogdPayType.SetupEditor
        With ogdPayType
            If e.Col = .Cols("FTPayUrsValue").Index Then
                Dim oControlEdit As New TextBox
                .Cols("FTPayUrsValue").Editor = oControlEdit
            End If
        End With
    End Sub

    Private Sub ogdPayType_AfterEdit(sender As Object, e As RowColEventArgs) Handles ogdPayType.AfterEdit
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oSQL As New StringBuilder

            Dim oConGrid As C1FlexGrid = CType(sender, C1FlexGrid)
            Dim tGetUValue As String = ogdPayType(oConGrid.Row, oConGrid.Col)
            Dim tGetName As String = ogdPayType(oConGrid.Row, 1)
            oSQL.AppendLine("UPDATE TLNKMappingPayment SET FTPayUrsValue = '" & tGetUValue & "'")
            oSQL.AppendLine("WHERE FTRcvCode ='" & tGetName & "' ")

            If oDatabase.C_CALnExecuteNonQuery(oSQL.ToString) = 1 Then
                MsgBox("ปรับปรุงข้อมูลเรียบร้อย")
            End If
        Catch ex As Exception

        End Try
    End Sub


End Class
