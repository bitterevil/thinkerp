﻿Imports System.IO

Public Class cControls
    Private oW_ListCtl As New Hashtable
    Private oW_DesCtl As New Hashtable
    Private tW_CurDesClt As String = ""
    Public Shared Property tAppPath As String = Application.StartupPath
    Public Enum eType
        [Connection] = 1
        [Configuration] = 2
        [Other] = 3
        [sFTP] = 4 'Add sFTP Config '*CH 22-06-2017
    End Enum

    Sub New()
        'Load Caption
        oW_DesCtl.Add(eType.Connection, Split("เลือกการเชื่อมต่อที่คุณต้องการปรับปรุง;Select the connection for which you want to update.", ";")(cCNVB.nVB_CutLng - 1))
        oW_DesCtl.Add(eType.Configuration, Split("เลือกการเชื่อมต่อข้อมูลไปยัง Web API ;Select the connection for Web Api.", ";")(cCNVB.nVB_CutLng - 1))
        oW_DesCtl.Add(eType.Other, Split("การจัดการข้อมูล;Mapping", ";")(cCNVB.nVB_CutLng - 1))
        '   oW_DesCtl.Add(eType.sFTP, Split("sFTP;sFTP", ";")(cCNVB.nVB_CutLng - 1)) 'Add sFTP Config '*CH 22-06-2017

        'Load Instance Menu
        oW_ListCtl.Add(eType.Connection, New cUsrConnection)
        oW_ListCtl.Add(eType.Configuration, New cUrsConfig)
        oW_ListCtl.Add(eType.Other, New cUsrMappingBranch)
        ' oW_ListCtl.Add(eType.Configuration, New cUsrConfiguration)
        ' oW_ListCtl.Add(eType.Other, New cUsrMapping)
        ' oW_ListCtl.Add(eType.sFTP, New cUsrFTP)

        'Load Dts Config ถ้ากรณีที่มีไฟล์ Config 
        If File.Exists(Application.StartupPath & "\Config\AdaConfig.xml") = True Then
            Me.W_SETxDataCtl()
        End If

    End Sub

    Private Sub W_SetxCurDes(ByVal ptDes As String)
        Me.tW_CurDesClt = ptDes
    End Sub

    Public Function W_GETtCurDes() As String
        Return Me.tW_CurDesClt
    End Function

    Public Sub W_GETxCurCtl(ByVal poControl As Control, ByVal pnIndex As eType)
        If Not Me.oW_ListCtl(pnIndex) Is Nothing Then
            poControl.Controls.Clear()
            poControl.Controls.Add(Me.oW_ListCtl(pnIndex))
            Me.W_SetxCurDes(Me.oW_DesCtl(pnIndex))
        End If
    End Sub


    'รับค่าจาก File Config มาโยนใส่ตัวแปร แล้ว เก็บค่าเอาไว้ 
    Public Sub W_SETxDataCtl()
        Dim oObjConn = oW_ListCtl(eType.Connection)
        'Set Connection
        With oObjConn
            .W_DATtServer = AdaConfig.cConfig.oConnSource.tServer
            .W_DATtUser = AdaConfig.cConfig.oConnSource.tUser
            .W_DATtPasswd = AdaConfig.cConfig.oConnSource.tPassword
            .W_DATtDbName = AdaConfig.cConfig.oConnSource.tCatalog
            .W_DATnTimeOut = AdaConfig.cConfig.oConnSource.nTimeOut
        End With

        'Member Database *CH 18-10-2014
        Dim oObjMemConn = oW_ListCtl(eType.Connection)
        'Set Connection
        With oObjMemConn
            .W_DATtMemServer = AdaConfig.cConfig.oMemConnSource.tServer
            .W_DATtMemUser = AdaConfig.cConfig.oMemConnSource.tUser
            .W_DATtMemPasswd = AdaConfig.cConfig.oMemConnSource.tPassword
            .W_DATtMemDbName = AdaConfig.cConfig.oMemConnSource.tCatalog
            .W_DATnMemTimeOut = AdaConfig.cConfig.oMemConnSource.nTimeOut
        End With



        'Set ConfigAPI
        Dim oObjAPI = oW_ListCtl(eType.Configuration)

        With oObjAPI
            .W_DATtUrl = AdaConfig.cConfig.oConfigAPI.tUrlSale
            .W_DATtMethod = AdaConfig.cConfig.oConfigAPI.tMethod
            .W_DATtContent = AdaConfig.cConfig.oConfigAPI.tContentType
            .W_DATtAppKey = AdaConfig.cConfig.oConfigAPI.tAppKey
            .W_DATtUrlReturn = AdaConfig.cConfig.oConfigAPI.tUrlReturn
            .W_DATtUrlReturnAll = AdaConfig.cConfig.oConfigAPI.tUrlReturnAll
        End With

        'Set Config
        '  Dim oObjConf = oW_ListCtl(eType.Configuration)
        'With oObjConf
        '    .W_DATtPathInbox = AdaConfig.cConfig.oConfigXml.tInbox
        '    .W_DATtPathBackup = AdaConfig.cConfig.oConfigXml.tBackup
        '    .W_DATtPathLog = AdaConfig.cConfig.oConfigXml.tLog
        '    .W_DATtPathOutbox = AdaConfig.cConfig.oConfigXml.tOutbox

        'End With

        'sFTP '*CH 22-06-2017
        'With oW_ListCtl(eType.sFTP)
        '    .W_DATtHostName = AdaConfig.cConfig.oConfigXml.tHostName
        '    .W_DATtUsrName = AdaConfig.cConfig.oConfigXml.tUserName
        '    .W_DATtPwd = AdaConfig.cConfig.oConfigXml.tPassword
        '    .W_DATtPortNum = AdaConfig.cConfig.oConfigXml.tPortNumber
        '    .W_DATtInBound = AdaConfig.cConfig.oConfigXml.tInFolder
        '    .W_DATtOutBound = AdaConfig.cConfig.oConfigXml.tOutFolder
        '    .W_DATtReconcile = AdaConfig.cConfig.oConfigXml.tReconcile
        '    .W_DATtCashTnf = AdaConfig.cConfig.oConfigXml.tCashTransfer
        '    .W_DATtExpense = AdaConfig.cConfig.oConfigXml.tExpense
        'End With
    End Sub

    Public Function W_DATbChkDb() As Boolean
        W_DATbChkDb = False
        Dim oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
        Dim tConn As String = String.Format("Data Source={0};User ID={1};Password={2};Initial Catalog={3};Persist Security Info=True;Connect Timeout=15",
                                          oObj.W_DATtServer, oObj.W_DATtUser, oObj.W_DATtPasswd, oObj.W_DATtDbName)
        Using oSQLConn As New SqlClient.SqlConnection(tConn)
            Try
                oSQLConn.Open()
                W_DATbChkDb = True
            Catch ex As Exception
            End Try
        End Using
    End Function


    'Save Xml file 
    Public Sub W_DATxSaveConfig()
        Try

            'เช็คว่ามีโฟลเดอไหม ถ้าไม่มีให้สร้าง
            If Directory.Exists(tAppPath & "\Config") = False Then
                Directory.CreateDirectory(tAppPath & "\Config")
            End If

            Dim oXEAdaConfig As XElement =
                  <config>
                      <Application User="EUrhNOJbKWw=" Pass="EUrhNOJbKWw=" Language="2"></Application>
                      <Connection Server="(local)\SQLEXPRESS" User="QSWVRqteAVo=" Password="foKZD3M9rwl1cFlJgFuA4w==" Catalog="master" TimeOut="500"></Connection>
                      <ConnectionMem Server="(local)\SQLEXPRESS" User="QSWVRqteAVo=" Password="foKZD3M9rwl1cFlJgFuA4w==" Catalog="master" TimeOut="500"></ConnectionMem>
                      <Separator Index="0" Define=""></Separator>
                  </config>
            '<config>
            '    <Application User="EUrhNOJbKWw=" Pass="EUrhNOJbKWw=" Language="2"></Application>
            '    <Connection Server="(local)\SQLEXPRESS" User="QSWVRqteAVo=" Password="foKZD3M9rwl1cFlJgFuA4w==" Catalog="master" TimeOut="500"></Connection>
            '    <ConnectionMem Server="(local)\SQLEXPRESS" User="QSWVRqteAVo=" Password="foKZD3M9rwl1cFlJgFuA4w==" Catalog="master" TimeOut="500"></ConnectionMem>
            '    <Separator Index="0" Define="0"></Separator>
            '    <Import Path="Import.xml"></Import>
            '    <Export Path="Export.xml"></Export>
            '    <FTP Path="sFTP.xml"></FTP>
            '</config>

            Dim oAdaConfigXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEAdaConfig)
            oAdaConfigXml.Save(tAppPath & "\Config\AdaConfig.xml")
            oAdaConfigXml = Nothing
            oXEAdaConfig = Nothing

            Dim oObj
            oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
            'Set ค่าให้กับ xml
            With AdaConfig.cConfig.oConnSource
                .tServer = oObj.W_DATtServer
                .tUser = oObj.W_DATtUser
                .tPassword = oObj.W_DATtPasswd
                .tCatalog = oObj.W_DATtDbName
                .nTimeOut = oObj.W_DATnTimeOut
            End With
            'Member Database *CH 18-10-2014
            With AdaConfig.cConfig.oMemConnSource
                .tServer = oObj.W_DATtMemServer
                .tUser = oObj.W_DATtMemUser
                .tPassword = oObj.W_DATtMemPasswd
                .tCatalog = oObj.W_DATtMemDbName
                .nTimeOut = oObj.W_DATnMemTimeOut
            End With
            AdaConfig.cConfig.C_SETbUpdateXml()

            'Mapping 
            'oObj = CType(Me.oW_ListCtl.Item(eType.Other), cUsrMapping)
            'With AdaConfig.cConfig.oSeparator
            '    .nIndex = oObj.W_GETnIndex
            '    .tDefine = oObj.W_GETtDefine
            'End With




            Dim oXeConnectAPI As XElement =
                   <Configurations>
                       <APIDetail Method="XXX" ContentType="XXX" ApplicationKey=""></APIDetail>
                       <APIUrlSale URL="XXX"></APIUrlSale>
                       <APIUrlReturn URL="XXX"></APIUrlReturn>
                       <APIUrlReturnAll URL="XXX"></APIUrlReturnAll>
                       <TypeReturn Type="XXX"></TypeReturn>
                   </Configurations>
            '<Configurations>
            '    <APIUrl URL="XXX" Method="XXX" ContentType="XXX" ApplicationKey=""></APIUrl>
            '</Configurations>
            Dim oAdaConfigAPI As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXeConnectAPI)
            oAdaConfigAPI.Save(tAppPath & "\Config\AdaConfigAPI.xml")
            oAdaConfigAPI = Nothing
            oAdaConfigAPI = Nothing

            Dim oConfigAPI = CType(Me.oW_ListCtl.Item(eType.Configuration), cUrsConfig)
            With AdaConfig.cConfig.oConfigAPI
                .tUrlSale = oConfigAPI.W_DATtUrl
                .tUrlReturn = oConfigAPI.W_DATtUrlReturn
                .tUrlReturnAll = oConfigAPI.W_DATtUrlReturnAll
                .tMethod = oConfigAPI.W_DATtMethod
                .tContentType = oConfigAPI.W_DATtContent
                .tAppKey = oConfigAPI.W_DATtAppKey
                .tTypeReturn = oConfigAPI.W_DATtType
            End With
            AdaConfig.cConfig.C_SETbUpdateXmlAPI()

            'Set Config API 
            '   oObj = CType(Me.oW_ListCtl.Item(eType.Configuration), cUrsConfig)
            ' AdaConfig.cConfig.C_CALxUpdateExport("")


            'Set Configuration
            '  oObj = CType(Me.oW_ListCtl.Item(eType.Configuration), cUsrConfiguration)
            '    AdaConfig.cConfig.C_CALxUpdateImport(oObj.W_DATtPathInbox, oObj.W_DATtPathLog, oObj.W_DATtPathBackup)
            '  AdaConfig.cConfig.C_CALxUpdateExport(oObj.W_DATtPathOutbox)


            'Set sFTP Config '*CH 22-06-2017
            'oObj = CType(Me.oW_ListCtl.Item(eType.sFTP), cUsrFTP)
            'AdaConfig.cConfig.C_CALxUpdateFTP(oObj.W_DATtHostName, oObj.W_DATtUsrName, oObj.W_DATtPwd, oObj.W_DATtPortNum,
            '                                  oObj.W_DATtInBound, oObj.W_DATtOutBound, oObj.W_DATtReconcile, oObj.W_DATtCashTnf, oObj.W_DATtExpense)

        Catch ex As Exception
        End Try
    End Sub

    Public Function W_DATbConnChage() As Boolean
        W_DATbConnChage = True
        Dim oObj
        oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
        If AdaConfig.cConfig.oConnSource.tServer = oObj.W_DATtServer And AdaConfig.cConfig.oConnSource.tUser = oObj.W_DATtUser And AdaConfig.cConfig.oConnSource.tPassword = oObj.W_DATtPasswd And AdaConfig.cConfig.oConnSource.tCatalog = oObj.W_DATtDbName And AdaConfig.cConfig.oConnSource.nTimeOut = oObj.W_DATnTimeOut Then
            W_DATbConnChage = False
        End If
    End Function

    'Member Database '*CH 18-10-2014
    Public Function W_DATbMemConnChage() As Boolean
        W_DATbMemConnChage = True
        Dim oObj
        oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
        If AdaConfig.cConfig.oMemConnSource.tServer = oObj.W_DATtMemServer And AdaConfig.cConfig.oMemConnSource.tUser = oObj.W_DATtMemUser And AdaConfig.cConfig.oMemConnSource.tPassword = oObj.W_DATtMemPasswd And AdaConfig.cConfig.oMemConnSource.tCatalog = oObj.W_DATtMemDbName And AdaConfig.cConfig.oMemConnSource.nTimeOut = oObj.W_DATnMemTimeOut Then
            W_DATbMemConnChage = False
        End If
    End Function

    ' Public Function W_GEToMapping() As DataTable
    ' Dim oObj
    '  oObj = CType(Me.oW_ListCtl.Item(eType.Other), cUsrMapping)
    'Return oObj.W_GEToMapping
    'End Function

End Class

