﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cUrsConfig
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.otbUrl = New System.Windows.Forms.TextBox()
        Me.olaRe = New System.Windows.Forms.Label()
        Me.otbUrlRe = New System.Windows.Forms.TextBox()
        Me.olaUrlReall = New System.Windows.Forms.Label()
        Me.otbReAll = New System.Windows.Forms.TextBox()
        Me.olaMethod = New System.Windows.Forms.Label()
        Me.otbMethod = New System.Windows.Forms.TextBox()
        Me.olaCtype = New System.Windows.Forms.Label()
        Me.otbCtype = New System.Windows.Forms.TextBox()
        Me.olaAppKey = New System.Windows.Forms.Label()
        Me.otbAppKey = New System.Windows.Forms.TextBox()
        Me.olaUrll = New System.Windows.Forms.Label()
        Me.otbUrlReAll = New System.Windows.Forms.TextBox()
        Me.olaUrl = New System.Windows.Forms.Label()
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.olaTitleType = New System.Windows.Forms.Label()
        Me.opnPtype = New System.Windows.Forms.Panel()
        Me.orbReturn = New System.Windows.Forms.RadioButton()
        Me.orbReturnBill = New System.Windows.Forms.RadioButton()
        Me.ogbMain = New System.Windows.Forms.GroupBox()
        Me.opnMain.SuspendLayout()
        Me.opnPtype.SuspendLayout()
        Me.ogbMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'otbUrl
        '
        Me.otbUrl.Location = New System.Drawing.Point(26, 17)
        Me.otbUrl.Name = "otbUrl"
        Me.otbUrl.Size = New System.Drawing.Size(398, 22)
        Me.otbUrl.TabIndex = 1
        '
        'olaRe
        '
        Me.olaRe.AutoSize = True
        Me.olaRe.Location = New System.Drawing.Point(26, 39)
        Me.olaRe.Name = "olaRe"
        Me.olaRe.Size = New System.Drawing.Size(83, 15)
        Me.olaRe.TabIndex = 10
        Me.olaRe.Text = "URL : Return"
        '
        'otbUrlRe
        '
        Me.otbUrlRe.Location = New System.Drawing.Point(26, 57)
        Me.otbUrlRe.Name = "otbUrlRe"
        Me.otbUrlRe.Size = New System.Drawing.Size(398, 22)
        Me.otbUrlRe.TabIndex = 8
        '
        'olaUrlReall
        '
        Me.olaUrlReall.AutoSize = True
        Me.olaUrlReall.Location = New System.Drawing.Point(26, 82)
        Me.olaUrlReall.Name = "olaUrlReall"
        Me.olaUrlReall.Size = New System.Drawing.Size(101, 16)
        Me.olaUrlReall.TabIndex = 13
        Me.olaUrlReall.Text = "URL : Return All"
        '
        'otbReAll
        '
        Me.otbReAll.Location = New System.Drawing.Point(26, 101)
        Me.otbReAll.Name = "otbReAll"
        Me.otbReAll.Size = New System.Drawing.Size(398, 22)
        Me.otbReAll.TabIndex = 11
        '
        'olaMethod
        '
        Me.olaMethod.AutoSize = True
        Me.olaMethod.Location = New System.Drawing.Point(26, 125)
        Me.olaMethod.Name = "olaMethod"
        Me.olaMethod.Size = New System.Drawing.Size(53, 16)
        Me.olaMethod.TabIndex = 2
        Me.olaMethod.Text = "Method"
        '
        'otbMethod
        '
        Me.otbMethod.Enabled = False
        Me.otbMethod.Location = New System.Drawing.Point(26, 144)
        Me.otbMethod.Name = "otbMethod"
        Me.otbMethod.Size = New System.Drawing.Size(398, 22)
        Me.otbMethod.TabIndex = 3
        Me.otbMethod.Text = "POST"
        '
        'olaCtype
        '
        Me.olaCtype.AutoSize = True
        Me.olaCtype.Location = New System.Drawing.Point(26, 169)
        Me.olaCtype.Name = "olaCtype"
        Me.olaCtype.Size = New System.Drawing.Size(89, 16)
        Me.olaCtype.TabIndex = 4
        Me.olaCtype.Text = "Content-Type"
        '
        'otbCtype
        '
        Me.otbCtype.Enabled = False
        Me.otbCtype.Location = New System.Drawing.Point(26, 189)
        Me.otbCtype.Name = "otbCtype"
        Me.otbCtype.Size = New System.Drawing.Size(398, 22)
        Me.otbCtype.TabIndex = 5
        Me.otbCtype.Text = "application/json"
        '
        'olaAppKey
        '
        Me.olaAppKey.AutoSize = True
        Me.olaAppKey.Location = New System.Drawing.Point(26, 213)
        Me.olaAppKey.Name = "olaAppKey"
        Me.olaAppKey.Size = New System.Drawing.Size(102, 16)
        Me.olaAppKey.TabIndex = 6
        Me.olaAppKey.Text = "Application-Key"
        '
        'otbAppKey
        '
        Me.otbAppKey.Location = New System.Drawing.Point(26, 233)
        Me.otbAppKey.Name = "otbAppKey"
        Me.otbAppKey.Size = New System.Drawing.Size(398, 22)
        Me.otbAppKey.TabIndex = 7
        '
        'olaUrll
        '
        Me.olaUrll.AutoSize = True
        Me.olaUrll.Location = New System.Drawing.Point(26, 0)
        Me.olaUrll.Name = "olaUrll"
        Me.olaUrll.Size = New System.Drawing.Size(72, 16)
        Me.olaUrll.TabIndex = 12
        Me.olaUrll.Text = "URL : Sale"
        '
        'otbUrlReAll
        '
        Me.otbUrlReAll.Location = New System.Drawing.Point(3, -177)
        Me.otbUrlReAll.Name = "otbUrlReAll"
        Me.otbUrlReAll.Size = New System.Drawing.Size(16, 22)
        Me.otbUrlReAll.TabIndex = 9
        '
        'olaUrl
        '
        Me.olaUrl.AutoSize = True
        Me.olaUrl.Location = New System.Drawing.Point(26, -180)
        Me.olaUrl.Name = "olaUrl"
        Me.olaUrl.Size = New System.Drawing.Size(72, 16)
        Me.olaUrl.TabIndex = 0
        Me.olaUrl.Text = "URL : Sale"
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 2
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.938272!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 95.06173!))
        Me.opnMain.Controls.Add(Me.olaUrl, 1, 5)
        Me.opnMain.Controls.Add(Me.otbUrlReAll, 0, 5)
        Me.opnMain.Controls.Add(Me.olaTitleType, 1, 22)
        Me.opnMain.Controls.Add(Me.otbAppKey, 1, 21)
        Me.opnMain.Controls.Add(Me.olaAppKey, 1, 20)
        Me.opnMain.Controls.Add(Me.otbCtype, 1, 19)
        Me.opnMain.Controls.Add(Me.olaCtype, 1, 18)
        Me.opnMain.Controls.Add(Me.otbMethod, 1, 17)
        Me.opnMain.Controls.Add(Me.olaMethod, 1, 16)
        Me.opnMain.Controls.Add(Me.otbReAll, 1, 15)
        Me.opnMain.Controls.Add(Me.olaUrlReall, 1, 14)
        Me.opnMain.Controls.Add(Me.otbUrlRe, 1, 13)
        Me.opnMain.Controls.Add(Me.olaRe, 1, 12)
        Me.opnMain.Controls.Add(Me.otbUrl, 1, 11)
        Me.opnMain.Controls.Add(Me.olaUrll, 1, 0)
        Me.opnMain.Controls.Add(Me.opnPtype, 1, 23)
        Me.opnMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.opnMain.Location = New System.Drawing.Point(6, 21)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 24
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.67257!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51.0!))
        Me.opnMain.Size = New System.Drawing.Size(474, 323)
        Me.opnMain.TabIndex = 0
        '
        'olaTitleType
        '
        Me.olaTitleType.AutoSize = True
        Me.olaTitleType.Location = New System.Drawing.Point(26, 256)
        Me.olaTitleType.Name = "olaTitleType"
        Me.olaTitleType.Size = New System.Drawing.Size(81, 16)
        Me.olaTitleType.TabIndex = 14
        Me.olaTitleType.Text = "Export Type"
        '
        'opnPtype
        '
        Me.opnPtype.Controls.Add(Me.orbReturn)
        Me.opnPtype.Controls.Add(Me.orbReturnBill)
        Me.opnPtype.Location = New System.Drawing.Point(26, 275)
        Me.opnPtype.Name = "opnPtype"
        Me.opnPtype.Size = New System.Drawing.Size(398, 16)
        Me.opnPtype.TabIndex = 15
        '
        'orbReturn
        '
        Me.orbReturn.AutoSize = True
        Me.orbReturn.Location = New System.Drawing.Point(90, -3)
        Me.orbReturn.Name = "orbReturn"
        Me.orbReturn.Size = New System.Drawing.Size(65, 20)
        Me.orbReturn.TabIndex = 15
        Me.orbReturn.TabStop = True
        Me.orbReturn.Tag = "2;คืนบางรายการ;Return"
        Me.orbReturn.Text = "Return"
        Me.orbReturn.UseVisualStyleBackColor = True
        '
        'orbReturnBill
        '
        Me.orbReturnBill.AutoSize = True
        Me.orbReturnBill.Location = New System.Drawing.Point(3, -3)
        Me.orbReturnBill.Name = "orbReturnBill"
        Me.orbReturnBill.Size = New System.Drawing.Size(83, 20)
        Me.orbReturnBill.TabIndex = 14
        Me.orbReturnBill.TabStop = True
        Me.orbReturnBill.Tag = "2;คืนทั้งบิล;ReturnBill"
        Me.orbReturnBill.Text = "ReturnBill"
        Me.orbReturnBill.UseVisualStyleBackColor = True
        '
        'ogbMain
        '
        Me.ogbMain.Controls.Add(Me.opnMain)
        Me.ogbMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!)
        Me.ogbMain.Location = New System.Drawing.Point(14, 3)
        Me.ogbMain.Name = "ogbMain"
        Me.ogbMain.Size = New System.Drawing.Size(455, 321)
        Me.ogbMain.TabIndex = 1
        Me.ogbMain.TabStop = False
        Me.ogbMain.Text = "Config Export"
        '
        'cUrsConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ogbMain)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Name = "cUrsConfig"
        Me.Size = New System.Drawing.Size(486, 326)
        Me.opnMain.ResumeLayout(False)
        Me.opnMain.PerformLayout()
        Me.opnPtype.ResumeLayout(False)
        Me.opnPtype.PerformLayout()
        Me.ogbMain.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents otbUrl As TextBox
    Friend WithEvents olaRe As Label
    Friend WithEvents otbUrlRe As TextBox
    Friend WithEvents olaUrlReall As Label
    Friend WithEvents otbReAll As TextBox
    Friend WithEvents olaMethod As Label
    Friend WithEvents otbMethod As TextBox
    Friend WithEvents olaCtype As Label
    Friend WithEvents otbCtype As TextBox
    Friend WithEvents olaAppKey As Label
    Friend WithEvents otbAppKey As TextBox
    Friend WithEvents olaUrll As Label
    Friend WithEvents otbUrlReAll As TextBox
    Friend WithEvents olaUrl As Label
    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents olaTitleType As Label
    Friend WithEvents opnPtype As Panel
    Friend WithEvents orbReturn As RadioButton
    Friend WithEvents orbReturnBill As RadioButton
    Friend WithEvents ogbMain As GroupBox
End Class
