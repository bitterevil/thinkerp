﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cUsrMappingBranch
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cUsrMappingBranch))
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.opnPay = New System.Windows.Forms.Panel()
        Me.ogdPayType = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.olaBranch = New System.Windows.Forms.Label()
        Me.opnBranch = New System.Windows.Forms.Panel()
        Me.ogdBranch = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.olaPayType = New System.Windows.Forms.Label()
        Me.opnMain.SuspendLayout()
        Me.opnPay.SuspendLayout()
        CType(Me.ogdPayType, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.opnBranch.SuspendLayout()
        CType(Me.ogdBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 2
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.790123!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 93.20988!))
        Me.opnMain.Controls.Add(Me.opnPay, 1, 4)
        Me.opnMain.Controls.Add(Me.olaBranch, 1, 1)
        Me.opnMain.Controls.Add(Me.opnBranch, 1, 2)
        Me.opnMain.Controls.Add(Me.olaPayType, 1, 3)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 5
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.14286!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.85714!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 134.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 151.0!))
        Me.opnMain.Size = New System.Drawing.Size(486, 327)
        Me.opnMain.TabIndex = 0
        '
        'opnPay
        '
        Me.opnPay.Controls.Add(Me.ogdPayType)
        Me.opnPay.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnPay.Location = New System.Drawing.Point(35, 178)
        Me.opnPay.Name = "opnPay"
        Me.opnPay.Size = New System.Drawing.Size(448, 146)
        Me.opnPay.TabIndex = 2
        '
        'ogdPayType
        '
        Me.ogdPayType.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.ogdPayType.ColumnInfo = resources.GetString("ogdPayType.ColumnInfo")
        Me.ogdPayType.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdPayType.Location = New System.Drawing.Point(0, 0)
        Me.ogdPayType.Name = "ogdPayType"
        Me.ogdPayType.Rows.DefaultSize = 21
        Me.ogdPayType.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange
        Me.ogdPayType.Size = New System.Drawing.Size(448, 146)
        Me.ogdPayType.StyleInfo = resources.GetString("ogdPayType.StyleInfo")
        Me.ogdPayType.TabIndex = 0
        Me.ogdPayType.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'olaBranch
        '
        Me.olaBranch.AutoSize = True
        Me.olaBranch.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaBranch.Location = New System.Drawing.Point(35, 7)
        Me.olaBranch.Name = "olaBranch"
        Me.olaBranch.Size = New System.Drawing.Size(56, 16)
        Me.olaBranch.TabIndex = 0
        Me.olaBranch.Text = "Branch :"
        '
        'opnBranch
        '
        Me.opnBranch.Controls.Add(Me.ogdBranch)
        Me.opnBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnBranch.Location = New System.Drawing.Point(35, 26)
        Me.opnBranch.Name = "opnBranch"
        Me.opnBranch.Size = New System.Drawing.Size(448, 128)
        Me.opnBranch.TabIndex = 1
        '
        'ogdBranch
        '
        Me.ogdBranch.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.ogdBranch.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdBranch.ColumnInfo = resources.GetString("ogdBranch.ColumnInfo")
        Me.ogdBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdBranch.HighLight = C1.Win.C1FlexGrid.HighLightEnum.WithFocus
        Me.ogdBranch.Location = New System.Drawing.Point(0, 0)
        Me.ogdBranch.Name = "ogdBranch"
        Me.ogdBranch.Rows.DefaultSize = 21
        Me.ogdBranch.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange
        Me.ogdBranch.Size = New System.Drawing.Size(448, 128)
        Me.ogdBranch.StyleInfo = resources.GetString("ogdBranch.StyleInfo")
        Me.ogdBranch.TabIndex = 0
        Me.ogdBranch.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'olaPayType
        '
        Me.olaPayType.AutoSize = True
        Me.olaPayType.Location = New System.Drawing.Point(35, 157)
        Me.olaPayType.Name = "olaPayType"
        Me.olaPayType.Size = New System.Drawing.Size(67, 16)
        Me.olaPayType.TabIndex = 2
        Me.olaPayType.Text = "Payment :"
        '
        'cUsrMappingBranch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.opnMain)
        Me.Name = "cUsrMappingBranch"
        Me.Size = New System.Drawing.Size(486, 327)
        Me.opnMain.ResumeLayout(False)
        Me.opnMain.PerformLayout()
        Me.opnPay.ResumeLayout(False)
        CType(Me.ogdPayType, System.ComponentModel.ISupportInitialize).EndInit()
        Me.opnBranch.ResumeLayout(False)
        CType(Me.ogdBranch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents opnPay As Panel
    Friend WithEvents ogdPayType As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents olaBranch As Label
    Friend WithEvents opnBranch As Panel
    Friend WithEvents ogdBranch As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents olaPayType As Label
End Class
