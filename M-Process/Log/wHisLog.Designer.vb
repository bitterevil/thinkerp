﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wHisLog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wHisLog))
        Me.otcExport = New System.Windows.Forms.TabControl()
        Me.otmTitle = New System.Windows.Forms.TabPage()
        Me.ogdLog = New System.Windows.Forms.DataGridView()
        Me.opnTable = New System.Windows.Forms.TableLayoutPanel()
        Me.opnBottom = New System.Windows.Forms.Panel()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.ocmClear = New System.Windows.Forms.Button()
        Me.odtDate = New System.Windows.Forms.DateTimePicker()
        Me.olaDate = New System.Windows.Forms.Label()
        Me.otcExport.SuspendLayout()
        Me.otmTitle.SuspendLayout()
        CType(Me.ogdLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.opnTable.SuspendLayout()
        Me.opnBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'otcExport
        '
        Me.otcExport.Controls.Add(Me.otmTitle)
        Me.otcExport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.otcExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!)
        Me.otcExport.Location = New System.Drawing.Point(27, 3)
        Me.otcExport.Name = "otcExport"
        Me.otcExport.SelectedIndex = 0
        Me.otcExport.Size = New System.Drawing.Size(736, 360)
        Me.otcExport.TabIndex = 0
        Me.otcExport.Tag = "2;ส่งออก;Export"
        '
        'otmTitle
        '
        Me.otmTitle.Controls.Add(Me.ogdLog)
        Me.otmTitle.Location = New System.Drawing.Point(4, 24)
        Me.otmTitle.Name = "otmTitle"
        Me.otmTitle.Padding = New System.Windows.Forms.Padding(3)
        Me.otmTitle.Size = New System.Drawing.Size(728, 332)
        Me.otmTitle.TabIndex = 0
        Me.otmTitle.Tag = "2;ส่งออก;Export"
        Me.otmTitle.Text = "Export"
        Me.otmTitle.UseVisualStyleBackColor = True
        '
        'ogdLog
        '
        Me.ogdLog.AllowUserToAddRows = False
        Me.ogdLog.AllowUserToDeleteRows = False
        Me.ogdLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdLog.Location = New System.Drawing.Point(3, 3)
        Me.ogdLog.Name = "ogdLog"
        Me.ogdLog.Size = New System.Drawing.Size(722, 326)
        Me.ogdLog.TabIndex = 0
        '
        'opnTable
        '
        Me.opnTable.ColumnCount = 3
        Me.opnTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.255208!))
        Me.opnTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 96.74479!))
        Me.opnTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 17.0!))
        Me.opnTable.Controls.Add(Me.otcExport, 1, 0)
        Me.opnTable.Controls.Add(Me.opnBottom, 1, 1)
        Me.opnTable.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnTable.Location = New System.Drawing.Point(0, 0)
        Me.opnTable.Name = "opnTable"
        Me.opnTable.RowCount = 2
        Me.opnTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.43374!))
        Me.opnTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.56627!))
        Me.opnTable.Size = New System.Drawing.Size(784, 415)
        Me.opnTable.TabIndex = 1
        '
        'opnBottom
        '
        Me.opnBottom.Controls.Add(Me.ocmClose)
        Me.opnBottom.Controls.Add(Me.ocmClear)
        Me.opnBottom.Controls.Add(Me.odtDate)
        Me.opnBottom.Controls.Add(Me.olaDate)
        Me.opnBottom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnBottom.Location = New System.Drawing.Point(27, 369)
        Me.opnBottom.Name = "opnBottom"
        Me.opnBottom.Size = New System.Drawing.Size(736, 43)
        Me.opnBottom.TabIndex = 1
        '
        'ocmClose
        '
        Me.ocmClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClose.Location = New System.Drawing.Point(632, 7)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(97, 27)
        Me.ocmClose.TabIndex = 5
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'ocmClear
        '
        Me.ocmClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClear.Location = New System.Drawing.Point(521, 7)
        Me.ocmClear.Name = "ocmClear"
        Me.ocmClear.Size = New System.Drawing.Size(105, 27)
        Me.ocmClear.TabIndex = 4
        Me.ocmClear.Tag = "2;ล้างค่า;Clear Log"
        Me.ocmClear.Text = "Clear Log"
        Me.ocmClear.UseVisualStyleBackColor = True
        '
        'odtDate
        '
        Me.odtDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.odtDate.Location = New System.Drawing.Point(395, 11)
        Me.odtDate.Name = "odtDate"
        Me.odtDate.Size = New System.Drawing.Size(101, 20)
        Me.odtDate.TabIndex = 3
        '
        'olaDate
        '
        Me.olaDate.AutoSize = True
        Me.olaDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaDate.Location = New System.Drawing.Point(322, 14)
        Me.olaDate.Name = "olaDate"
        Me.olaDate.Size = New System.Drawing.Size(67, 15)
        Me.olaDate.TabIndex = 0
        Me.olaDate.Tag = "2;วันที่;SelectDate"
        Me.olaDate.Text = "SelectDate"
        '
        'wHisLog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 415)
        Me.Controls.Add(Me.opnTable)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wHisLog"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ประวัติการใช้งาน;History Log"
        Me.Text = "History Log"
        Me.otcExport.ResumeLayout(False)
        Me.otmTitle.ResumeLayout(False)
        CType(Me.ogdLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.opnTable.ResumeLayout(False)
        Me.opnBottom.ResumeLayout(False)
        Me.opnBottom.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents otcExport As TabControl
    Friend WithEvents otmTitle As TabPage
    Friend WithEvents ogdLog As DataGridView
    Friend WithEvents opnTable As TableLayoutPanel
    Friend WithEvents opnBottom As Panel
    Friend WithEvents ocmClose As Button
    Friend WithEvents ocmClear As Button
    Friend WithEvents odtDate As DateTimePicker
    Friend WithEvents olaDate As Label
End Class
