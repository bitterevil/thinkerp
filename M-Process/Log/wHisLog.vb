﻿Imports System.ComponentModel
Imports System.IO

Public Class wHisLog
    Private Sub wHisLog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)

        W_SETxLoadLogfile()
    End Sub

    ''' <summary>
    ''' Spilt ค่า จาก Textfile มาใส่ใน Column Grid
    ''' </summary>
    Public Sub W_SETxLoadLogfile()
        Try
            Dim tDes As String = ""
            Dim atPdtRow As New ArrayList
            Dim oDbTbl As New DataTable
            Dim tSplie() As String
            oDbTbl = W_SEToDrow()

            Dim tPathName As String = ""
            Dim tPath As New DirectoryInfo(AdaConfig.cConfig.tAppPath & "\Log")
            Dim oGetFile As FileInfo() = tPath.GetFiles()

            For Each oFile As FileInfo In oGetFile
                tDes = ""
                ' Read Text file เพื่อเอาค่า Success มาใส่ใน Description
                Dim tReadText As String = File.ReadAllText(AdaConfig.cConfig.tAppPath & "\Log\" & oFile.Name)

                For Each tPdtRow As String In tReadText.Split(vbCr & vbLf)

                    If tPdtRow = vbLf Then
                        Exit For
                    Else
                        If Not String.IsNullOrEmpty(tPdtRow) Then
                            atPdtRow.Add(tPdtRow)
                        End If
                    End If

                Next

                tDes = Convert.ToString(atPdtRow(0))
                Dim oDatarow As DataRow = oDbTbl.NewRow()
                Dim oArr As Array

                oArr = Split(oFile.Name, "_")
                oDatarow("Date") = W_CONdConverttoDate(oArr(2))
                oDatarow("FileName") = oFile.Name
                'oDatarow("Description") = oArr(1)
                oDatarow("Description") = tDes
                oDatarow("Refer") = oFile.Name
                oDbTbl.Rows.Add(oDatarow)

                'Clear ค่า ArrayList ไม่ให้จำค่าเดิม
                atPdtRow.Clear()
            Next

            ' End If
            With ogdLog
                If .Rows.Count >= 0 Then
                    .DataSource = oDbTbl

                    If cCNVB.nVB_CutLng = 1 Then
                        .Columns("Date").HeaderText = "วันที่"
                        .Columns("FileName").HeaderText = "ชื่อ"
                        .Columns("Description").HeaderText = "คำอธิบาย"
                        .Columns("Refer").HeaderText = "ที่อยู่"
                    Else cCNVB.nVB_CutLng = 2
                        .Columns("Date").HeaderText = "Date"
                        .Columns("FileName").HeaderText = "FileName"
                        .Columns("Description").HeaderText = "Description"
                        .Columns("Refer").HeaderText = "Address"
                    End If
                    .Columns("Date").ReadOnly = True
                    .Columns("FileName").ReadOnly = True
                    .Columns("Description").ReadOnly = True
                    .Columns("Refer").ReadOnly = True
                    .Sort(ogdLog.Columns(1), ListSortDirection.Descending)
                    .Columns("Description").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .Columns("FileName").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .Columns("Refer").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    ogdLog.RowHeadersVisible = False

                    'สร้าง Column textbox 
                    Dim obtn As New DataGridViewButtonColumn()
                    If ogdLog.Columns("") Is Nothing Then
                        ogdLog.Columns.Add(obtn)
                    End If
                    obtn.Width = 20
                    obtn.Text = "..."
                    obtn.Name = ""
                    obtn.UseColumnTextForButtonValue = True




                End If

            End With

        Catch ex As Exception

        End Try
    End Sub

    Public Function W_SEToDrow() As DataTable
        Try

            Dim oDbtbl As New DataTable
            oDbtbl.Columns.Add("Date")
            oDbtbl.Columns.Add("FileName")
            oDbtbl.Columns.Add("Description")
            oDbtbl.Columns.Add("Refer")
            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function W_CONdConverttoDate(ByVal tFile As String) As String
        Try
            Dim oArr As Array
            oArr = Split(tFile, ".")

            'แปลง String กลับเป็นวันที่ 
            Dim tConFormat As String = DateTime.ParseExact(oArr(0), "yyyyMMddHHmm", New System.Globalization.CultureInfo("en-US"))
            Dim tDate As String = Convert.ToDateTime(tConFormat).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))

            Return tDate

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Sub ogdLog_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ogdLog.CellContentClick

        If e.ColumnIndex = 4 Then

            Dim tPath As String = AdaConfig.cConfig.tAppPath & "\Log\" + ogdLog.Rows(e.RowIndex).Cells(3).Value

            If System.IO.File.Exists(tPath) = True Then
                Diagnostics.Process.Start(tPath)
            Else
                cCNSP.SP_MSGnShowing(e.ColumnIndex = 4, cCNEN.eEN_MSGStyle.nEN_MSGWarn, True)
            End If
        End If

    End Sub

    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub



    Private Sub ocmClear_Click(sender As Object, e As EventArgs) Handles ocmClear.Click
        Try

            Dim tPath As New DirectoryInfo(AdaConfig.cConfig.tAppPath & "\Log")
            Dim oGetFile As FileInfo() = tPath.GetFiles()
            For Each oFile As FileInfo In oGetFile
                Dim tDel As String = AdaConfig.cConfig.tAppPath & "\Log\" & oFile.Name
                System.IO.File.Delete(tDel)
                ogdLog.ClearSelection()
            Next
            W_SETxLoadLogfile()
        Catch ex As Exception

        End Try
    End Sub
End Class