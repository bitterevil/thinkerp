﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wExports
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wExports))
        Me.ogdExport = New System.Windows.Forms.DataGridView()
        Me.olaTitleStatus = New System.Windows.Forms.Label()
        Me.opgProcess = New System.Windows.Forms.ProgressBar()
        Me.olaDate = New System.Windows.Forms.Label()
        Me.olaDocNo = New System.Windows.Forms.Label()
        Me.odtDateSearch = New System.Windows.Forms.DateTimePicker()
        Me.otbDockNo = New System.Windows.Forms.TextBox()
        Me.ocmSearch = New System.Windows.Forms.Button()
        Me.ocmProcess = New System.Windows.Forms.Button()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.obgWorker = New System.ComponentModel.BackgroundWorker()
        Me.olaDocProcess = New System.Windows.Forms.Label()
        Me.otcTabMain = New System.Windows.Forms.TabControl()
        Me.Sale = New System.Windows.Forms.TabPage()
        Me.tReturn = New System.Windows.Forms.TabPage()
        Me.ogdReturn = New System.Windows.Forms.DataGridView()
        CType(Me.ogdExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.otcTabMain.SuspendLayout()
        Me.Sale.SuspendLayout()
        Me.tReturn.SuspendLayout()
        CType(Me.ogdReturn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ogdExport
        '
        Me.ogdExport.AllowUserToAddRows = False
        Me.ogdExport.AllowUserToDeleteRows = False
        Me.ogdExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdExport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdExport.Location = New System.Drawing.Point(3, 3)
        Me.ogdExport.Name = "ogdExport"
        Me.ogdExport.Size = New System.Drawing.Size(602, 218)
        Me.ogdExport.TabIndex = 0
        '
        'olaTitleStatus
        '
        Me.olaTitleStatus.AutoSize = True
        Me.olaTitleStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaTitleStatus.Location = New System.Drawing.Point(9, 329)
        Me.olaTitleStatus.Name = "olaTitleStatus"
        Me.olaTitleStatus.Size = New System.Drawing.Size(62, 20)
        Me.olaTitleStatus.TabIndex = 1
        Me.olaTitleStatus.Tag = "2;สถานะ:;Status:"
        Me.olaTitleStatus.Text = "Status:"
        '
        'opgProcess
        '
        Me.opgProcess.Location = New System.Drawing.Point(12, 348)
        Me.opgProcess.Name = "opgProcess"
        Me.opgProcess.Size = New System.Drawing.Size(609, 26)
        Me.opgProcess.TabIndex = 2
        '
        'olaDate
        '
        Me.olaDate.AutoSize = True
        Me.olaDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaDate.Location = New System.Drawing.Point(55, 26)
        Me.olaDate.Name = "olaDate"
        Me.olaDate.Size = New System.Drawing.Size(55, 20)
        Me.olaDate.TabIndex = 3
        Me.olaDate.Tag = "2;วันที่ :;Date :"
        Me.olaDate.Text = "Date :"
        '
        'olaDocNo
        '
        Me.olaDocNo.AutoSize = True
        Me.olaDocNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaDocNo.Location = New System.Drawing.Point(247, 26)
        Me.olaDocNo.Name = "olaDocNo"
        Me.olaDocNo.Size = New System.Drawing.Size(105, 20)
        Me.olaDocNo.TabIndex = 4
        Me.olaDocNo.Tag = "2;เลขที่เอกสาร :;Doccument :"
        Me.olaDocNo.Text = "Doccument :"
        '
        'odtDateSearch
        '
        Me.odtDateSearch.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.odtDateSearch.Location = New System.Drawing.Point(101, 24)
        Me.odtDateSearch.Name = "odtDateSearch"
        Me.odtDateSearch.Size = New System.Drawing.Size(126, 24)
        Me.odtDateSearch.TabIndex = 5
        '
        'otbDockNo
        '
        Me.otbDockNo.Location = New System.Drawing.Point(331, 24)
        Me.otbDockNo.Name = "otbDockNo"
        Me.otbDockNo.Size = New System.Drawing.Size(156, 24)
        Me.otbDockNo.TabIndex = 6
        '
        'ocmSearch
        '
        Me.ocmSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!)
        Me.ocmSearch.Location = New System.Drawing.Point(493, 23)
        Me.ocmSearch.Name = "ocmSearch"
        Me.ocmSearch.Size = New System.Drawing.Size(100, 23)
        Me.ocmSearch.TabIndex = 7
        Me.ocmSearch.Tag = "2;ค้นหา;Search"
        Me.ocmSearch.Text = "Search"
        Me.ocmSearch.UseVisualStyleBackColor = True
        '
        'ocmProcess
        '
        Me.ocmProcess.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ocmProcess.Location = New System.Drawing.Point(436, 380)
        Me.ocmProcess.Name = "ocmProcess"
        Me.ocmProcess.Size = New System.Drawing.Size(89, 31)
        Me.ocmProcess.TabIndex = 8
        Me.ocmProcess.Tag = "2;ดำเนินการ;Process"
        Me.ocmProcess.Text = "Process"
        Me.ocmProcess.UseVisualStyleBackColor = True
        '
        'ocmClose
        '
        Me.ocmClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ocmClose.Location = New System.Drawing.Point(531, 380)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(90, 31)
        Me.ocmClose.TabIndex = 9
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'obgWorker
        '
        Me.obgWorker.WorkerReportsProgress = True
        '
        'olaDocProcess
        '
        Me.olaDocProcess.AutoSize = True
        Me.olaDocProcess.Location = New System.Drawing.Point(61, 329)
        Me.olaDocProcess.Name = "olaDocProcess"
        Me.olaDocProcess.Size = New System.Drawing.Size(0, 18)
        Me.olaDocProcess.TabIndex = 10
        '
        'otcTabMain
        '
        Me.otcTabMain.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.otcTabMain.Controls.Add(Me.Sale)
        Me.otcTabMain.Controls.Add(Me.tReturn)
        Me.otcTabMain.Location = New System.Drawing.Point(12, 71)
        Me.otcTabMain.Multiline = True
        Me.otcTabMain.Name = "otcTabMain"
        Me.otcTabMain.SelectedIndex = 0
        Me.otcTabMain.Size = New System.Drawing.Size(616, 255)
        Me.otcTabMain.TabIndex = 11
        '
        'Sale
        '
        Me.Sale.Controls.Add(Me.ogdExport)
        Me.Sale.Location = New System.Drawing.Point(4, 27)
        Me.Sale.Name = "Sale"
        Me.Sale.Padding = New System.Windows.Forms.Padding(3)
        Me.Sale.Size = New System.Drawing.Size(608, 224)
        Me.Sale.TabIndex = 0
        Me.Sale.Tag = "2;ประเภทขาย;Sale"
        Me.Sale.Text = "ประเภทขาย"
        Me.Sale.UseVisualStyleBackColor = True
        '
        'tReturn
        '
        Me.tReturn.Controls.Add(Me.ogdReturn)
        Me.tReturn.Location = New System.Drawing.Point(4, 27)
        Me.tReturn.Name = "tReturn"
        Me.tReturn.Padding = New System.Windows.Forms.Padding(3)
        Me.tReturn.Size = New System.Drawing.Size(608, 224)
        Me.tReturn.TabIndex = 1
        Me.tReturn.Tag = "2;ประเภทคืน;Return"
        Me.tReturn.Text = "ประเภทคืน"
        Me.tReturn.UseVisualStyleBackColor = True
        '
        'ogdReturn
        '
        Me.ogdReturn.AllowUserToAddRows = False
        Me.ogdReturn.AllowUserToDeleteRows = False
        Me.ogdReturn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdReturn.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdReturn.Location = New System.Drawing.Point(3, 3)
        Me.ogdReturn.Name = "ogdReturn"
        Me.ogdReturn.Size = New System.Drawing.Size(602, 218)
        Me.ogdReturn.TabIndex = 0
        '
        'wExports
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(640, 421)
        Me.Controls.Add(Me.otcTabMain)
        Me.Controls.Add(Me.olaDocProcess)
        Me.Controls.Add(Me.ocmClose)
        Me.Controls.Add(Me.ocmProcess)
        Me.Controls.Add(Me.ocmSearch)
        Me.Controls.Add(Me.otbDockNo)
        Me.Controls.Add(Me.odtDateSearch)
        Me.Controls.Add(Me.olaDocNo)
        Me.Controls.Add(Me.olaDate)
        Me.Controls.Add(Me.opgProcess)
        Me.Controls.Add(Me.olaTitleStatus)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wExports"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ส่งออก;Export"
        Me.Text = "Export"
        CType(Me.ogdExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.otcTabMain.ResumeLayout(False)
        Me.Sale.ResumeLayout(False)
        Me.tReturn.ResumeLayout(False)
        CType(Me.ogdReturn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ogdExport As DataGridView
    Friend WithEvents olaTitleStatus As Label
    Friend WithEvents opgProcess As ProgressBar
    Friend WithEvents olaDate As Label
    Friend WithEvents olaDocNo As Label
    Friend WithEvents odtDateSearch As DateTimePicker
    Friend WithEvents otbDockNo As TextBox
    Friend WithEvents ocmSearch As Button
    Friend WithEvents ocmProcess As Button
    Friend WithEvents ocmClose As Button
    Friend WithEvents obgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents olaDocProcess As Label
    Friend WithEvents otcTabMain As TabControl
    Friend WithEvents Sale As TabPage
    Friend WithEvents tReturn As TabPage
    Friend WithEvents ogdReturn As DataGridView
End Class
