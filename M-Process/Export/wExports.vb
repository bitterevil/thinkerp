﻿Imports System.IO
Imports System.Text

Public Class wExports


    Dim tW_DateExport As String = Now.ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
    Dim oW_oDbtblTemp As New DataTable
    Dim oW_oDbtblTmpReturn As DataTable
    Dim tW_DocNo As String = ""
    Private nW_StatusAPI As String = 0
    '=== Caption EN|TH ==='

    Private tW_CapPackage As String = "เลือก|Package"
    Private tW_Time As String = "เวลา|Time"
    Private tW_Net As String = "ยอดขาย|Sale"


    ''' <summary>
    ''' สร้าง Column สำหรับ Table การขาย
    ''' </summary>
    ''' <returns></returns>
    Function W_SEToColumnPaymentSale() As DataTable
        Try

            Dim oDbtbl As New DataTable
            oDbtbl.Columns.Add("FTSelect", GetType(Boolean))
            oDbtbl.Columns.Add("FTShdDocNo", GetType(String))
            oDbtbl.Columns.Add("FTShdDocTime", GetType(String))
            oDbtbl.Columns.Add("FCShdGrand", GetType(String))

            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    ''' <summary>
    ''' สร้างคอลัมน์ สำหรับ Table การคืน
    ''' </summary>
    ''' <returns></returns>
    Function W_SEToColumnPaymentReturn() As DataTable
        Try

            Dim oDbtbl As New DataTable
            oDbtbl.Columns.Add("FTSelect", GetType(Boolean))
            oDbtbl.Columns.Add("FTShdDocNo", GetType(String))
            oDbtbl.Columns.Add("FTShdDocTime", GetType(String))
            oDbtbl.Columns.Add("FCShdGrand", GetType(String))

            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    ''' <summary>
    ''' Load ข้อมูล การขายลงใน Grid
    ''' </summary>
    ''' <param name="tDate"></param>
    Public Sub W_GETDataGrid(ByVal tDate As String)
        Try

            Dim oSQL As New StringBuilder
            Dim ockExp As New DataGridViewCheckBoxColumn()
            Dim oDatabase As New cDatabaseLocal

            Dim oDbtblSale As New DataTable
            Dim tCondition As String = ""

            oSQL.Clear()
            oSQL.AppendLine(" SELECT    FTShdDocNo, FTShdDocTime, FCShdGrand ")
            oSQL.AppendLine(" FROM TPSTSalHD ")
            oSQL.AppendLine(" WHERE  (CONVERT(VARCHAR, FDShdDocDate, 103) = '" & tDate & "' ) AND  (FTShdDocType = 1) ")
            If otbDockNo.Text <> "" Then
                oSQL.AppendLine(" AND (FTShdDocNo = '" & otbDockNo.Text.Trim & "') ")
            End If
            oDbtblSale = oDatabase.C_CALoExecuteReader(oSQL.ToString())

            If oDbtblSale.Rows.Count > 0 Then
                oW_oDbtblTemp = W_SEToColumnPaymentSale()
                For nRow As Integer = 0 To oDbtblSale.Rows.Count - 1
                    Dim oDatarow As DataRow = oW_oDbtblTemp.NewRow()
                    oDatarow("FTSelect") = True
                    oDatarow("FTShdDocNo") = oDbtblSale(nRow)("FTShdDocNo").ToString
                    oDatarow("FTShdDocTime") = oDbtblSale(nRow)("FTShdDocTime").ToString
                    oDatarow("FCShdGrand") = oDbtblSale(nRow)("FCShdGrand").ToString
                    oW_oDbtblTemp.Rows.Add(oDatarow)
                Next

            Else

                If cCNVB.nVB_CutLng = 1 Then

                    MsgBox("ไม่มีข้อมูลที่ค้นหา", MsgBoxStyle.Critical)
                Else cCNVB.nVB_CutLng = 2
                    MsgBox("Information not found.", MsgBoxStyle.Critical)
                End If



            End If

            ogdExport.DataSource = oW_oDbtblTemp
            With ogdExport
                If cCNVB.nVB_CutLng = 1 Then
                    .Columns("FTSelect").HeaderText = "เลือก"
                    .Columns("FTShdDocNo").HeaderText = "เลขที่เอกสาร"
                    .Columns("FTShdDocTime").HeaderText = "เวลา"
                    .Columns("FCShdGrand").HeaderText = "ยอดขาย"
                Else cCNVB.nVB_CutLng = 2
                    .Columns("FTSelect").HeaderText = "Package"
                    .Columns("FTShdDocNo").HeaderText = "Doccument"
                    .Columns("FTShdDocTime").HeaderText = "Time"
                    .Columns("FCShdGrand").HeaderText = "Sale"
                End If

                .Columns("FTShdDocNo").ReadOnly = True
                .Columns("FTShdDocTime").ReadOnly = True
                .Columns("FCShdGrand").ReadOnly = True
                ogdExport.RowHeadersVisible = False

                .Columns("FTShdDocNo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            End With

        Catch ex As Exception

        End Try
    End Sub

    Public Sub W_GETDataGridReturn(ByVal tDate As String)
        Try

            Dim oSQL As New StringBuilder
            Dim ockExp As New DataGridViewCheckBoxColumn()
            Dim oDatabase As New cDatabaseLocal

            Dim oDbtblSale As New DataTable
            Dim tCondition As String = ""

            oSQL.Clear()
            oSQL.AppendLine(" SELECT    FTShdDocNo, FTShdDocTime, FCShdGrand ")
            oSQL.AppendLine(" FROM TPSTSalHD ")
            oSQL.AppendLine(" WHERE  (CONVERT(VARCHAR, FDShdDocDate, 103) = '" & tDate & "' ) AND  (FTShdDocType = 9) ")

            If otbDockNo.Text <> "" Then
                oSQL.AppendLine(" AND (FTShdDocNo = '" & otbDockNo.Text.Trim & "') ")
            End If

            oDbtblSale = oDatabase.C_CALoExecuteReader(oSQL.ToString())
            If oDbtblSale.Rows.Count > 0 Then
                oW_oDbtblTmpReturn = W_SEToColumnPaymentReturn()
                For nRow As Integer = 0 To oDbtblSale.Rows.Count - 1
                    Dim oDatarow As DataRow = oW_oDbtblTmpReturn.NewRow()
                    oDatarow("FTSelect") = True
                    oDatarow("FTShdDocNo") = oDbtblSale(nRow)("FTShdDocNo").ToString
                    oDatarow("FTShdDocTime") = oDbtblSale(nRow)("FTShdDocTime").ToString
                    oDatarow("FCShdGrand") = oDbtblSale(nRow)("FCShdGrand").ToString
                    oW_oDbtblTmpReturn.Rows.Add(oDatarow)
                Next

            Else

                'If cCNVB.nVB_CutLng = 1 Then
                '    MsgBox("ไม่มีข้อมูลที่ค้นหา", MsgBoxStyle.Critical)
                'Else cCNVB.nVB_CutLng = 2
                '    MsgBox("Information not found.", MsgBoxStyle.Critical)
                'End If



            End If

            ogdReturn.DataSource = oW_oDbtblTmpReturn
            With ogdReturn
                If cCNVB.nVB_CutLng = 1 Then
                    .Columns("FTSelect").HeaderText = "เลือก"
                    .Columns("FTShdDocNo").HeaderText = "เลขที่เอกสาร"
                    .Columns("FTShdDocTime").HeaderText = "เวลา"
                    .Columns("FCShdGrand").HeaderText = "ยอดขาย"
                Else cCNVB.nVB_CutLng = 2
                    .Columns("FTSelect").HeaderText = "Package"
                    .Columns("FTShdDocNo").HeaderText = "Doccument"
                    .Columns("FTShdDocTime").HeaderText = "Time"
                    .Columns("FCShdGrand").HeaderText = "Sale"
                End If

                .Columns("FTShdDocNo").ReadOnly = True
                .Columns("FTShdDocTime").ReadOnly = True
                .Columns("FCShdGrand").ReadOnly = True
                ogdReturn.RowHeadersVisible = False
                .Columns("FTShdDocNo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With


        Catch ex As Exception

        End Try
    End Sub

    Private Sub wExports_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)
        W_SETxTimeFormat()
    End Sub

    Private Sub ocmSearch_Click(sender As Object, e As EventArgs) Handles ocmSearch.Click
        Try
            Dim tDate As String = Convert.ToDateTime(odtDateSearch.Value).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            W_GETDataGrid(tDate)
            W_GETDataGridReturn(tDate)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub

    Private Sub ocmProcess_Click(sender As Object, e As EventArgs) Handles ocmProcess.Click
        Try
            If Not obgWorker.IsBusy Then
                obgWorker.RunWorkerAsync()
            End If
        Catch ex As Exception

        End Try
    End Sub


    ''' <summary>
    ''' Process Export
    ''' </summary>
    Public Sub W_EXPxProcessApi()
        Try
            cExportSale.nC_Success = 0
            cExportSale.nC_Fail = 0

            Dim tStatusReturn As String = ""
            Dim tDate As String = Convert.ToDateTime(odtDateSearch.Value).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Dim oExpSale As New cExportSale
            Dim tSelect As String = ""
            Dim tType As String = ""
            tType = AdaConfig.cConfig.oConfigAPI.tTypeReturn

            'Export ประเภท ขาย 
            For nSelect As Integer = 0 To oW_oDbtblTemp.Rows.Count - 1
                ' ถ้ามีการเลือก ให้เข้าเงื่อนไข 
                If ogdExport.Rows(nSelect).Cells(0).Value = True Then
                    System.Threading.Thread.Sleep(1000)
                    obgWorker.ReportProgress(0)
                    'เงื่อนไขที่ Row ที่เท่าไหร่
                    tSelect = ogdExport.Rows(nSelect).Cells(0).Value
                    tW_DocNo = ogdExport.Rows(nSelect).Cells(1).Value

                    'Export ประเภทขาย
                    If oExpSale.C_EXPbExportSale(tDate, tW_DocNo, "Sale") = True Then
                        System.Threading.Thread.Sleep(100)
                        obgWorker.ReportProgress(100)
                        nW_StatusAPI = 1
                    Else
                        nW_StatusAPI = 0
                        Exit Sub
                    End If
                End If
            Next

            'Clear ค่า
            tSelect = ""
            tW_DocNo = ""

            For nRowReturn As Integer = 0 To oW_oDbtblTmpReturn.Rows.Count - 1
                If ogdReturn.Rows(nRowReturn).Cells(0).Value = True Then

                    System.Threading.Thread.Sleep(1000)
                    obgWorker.ReportProgress(0)
                    tSelect = ogdReturn.Rows(nRowReturn).Cells(0).Value
                    tW_DocNo = ogdReturn.Rows(nRowReturn).Cells(1).Value

                    'Export ประเภท คืนทั้ง Bill
                    If tType = 0 Then
                        tStatusReturn = "ReturnAll"
                    ElseIf tType = 1 Then
                        tStatusReturn = "Return"
                    Else
                        tStatusReturn = "ReturnAll"
                    End If

                    If oExpSale.C_EXPbExportSale(tDate, tW_DocNo, tStatusReturn) = True Then
                        'obgWorker.ReportProgress(100)
                        System.Threading.Thread.Sleep(100)
                        nW_StatusAPI = 1

                    Else
                        nW_StatusAPI = 0
                        Exit Sub
                    End If

                End If
            Next

            tStatusReturn = ""
        Catch ex As Exception

        End Try

    End Sub

    Private Sub obgWorker_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles obgWorker.DoWork
        W_EXPxProcessApi()
    End Sub

    Private Sub obgWorker_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles obgWorker.ProgressChanged
        Dim tStatusTxt As String = ""
        If cCNVB.nVB_CutLng = 1 Then
            tStatusTxt = "กำลังส่งออก เอกสารเลขที่ "
        ElseIf cCNVB.nVB_CutLng = 2 Then
            tStatusTxt = "Process No. "
        End If

        For i = 1 To 100
            'obgWorker.ReportProgress(100)
            ' System.Threading.Thread.Sleep(10)
            opgProcess.Value = i
            olaDocProcess.Text = tStatusTxt + tW_DocNo + " |" + CType(i, String) + " %"
            olaDocProcess.Refresh()
        Next

    End Sub

    Private Sub obgWorker_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles obgWorker.RunWorkerCompleted

        If cCNVB.nVB_CutLng = 1 Then
            ' สถานะการ Export  = 1 และ ค่าของแต่ละรายการที่ Fail = 0 
            If nW_StatusAPI = 1 And cExportSale.nC_Fail = 0 Then
                MsgBox("ส่งออกข้อมูลสำเร็จ", MsgBoxStyle.Information)
            ElseIf nW_StatusAPI = 0 Then
                'สำหรับ Write Log Fail กรณีเชื่อมต่อ API ไม่ได้ 
                cLog.C_CALxWriteLogFail("NotFoundApi")
                'สำหรับ Write Log Fail กรณี API มีปัญหาระหว่างทำงาน 
                cLog.C_CALxWriteLogExport("NotFoundApi", "", "", "", "")
                MsgBox("เกิดข้อผิดพลาด", MsgBoxStyle.Critical)
            Else
                MsgBox("ส่งออกผิดพลาด กรุณาตรวจสอบ Log ", MsgBoxStyle.Critical)
            End If

        ElseIf cCNVB.nVB_CutLng = 2 Then

            If nW_StatusAPI = 1 And cExportSale.nC_Fail = 0 Then
                MsgBox("Export Success", MsgBoxStyle.Information)
            ElseIf nW_StatusAPI = 0 Then
                'สำหรับ Write Log Fail กรณีเชื่อมต่อ API ไม่ได้ 
                cLog.C_CALxWriteLogFail("NotFoundApi")
                'สำหรับ Write Log Fail กรณี API มีปัญหาระหว่างทำงาน 
                cLog.C_CALxWriteLogExport("NotFoundApi", "", "", "", "")
                MsgBox("Export fail", MsgBoxStyle.Critical)
            Else
                MsgBox("Failed to Export Please check log File", MsgBoxStyle.Critical)
            End If

        End If

        W_SETxWriteSuccess()

        cExportSale.nC_CreateFile = 0
        cExportSale.nC_Success = 0
        cExportSale.nC_Fail = 0
    End Sub

    Public Sub W_SETxTimeFormat()
        odtDateSearch.Format = DateTimePickerFormat.Custom
        odtDateSearch.CustomFormat = "dd/MM/yyyy"
    End Sub


    ''' <summary>
    ''' เขียน จำนวนแถวที่ Success และ Fail 
    ''' </summary>
    Public Sub W_SETxWriteSuccess()
        Try
            Dim tExpSuccess As Integer = cExportSale.nC_Success
            Dim tExpFail As Integer = cExportSale.nC_Fail
            Dim oHDfile As New StringBuilder
            Dim tPathLog As String = AdaConfig.cConfig.tAppPath & "\Log\Process"
            Dim tPath As New DirectoryInfo(AdaConfig.cConfig.tAppPath & "\Log\Process")
            Dim oGetFile As FileInfo() = tPath.GetFiles()

            For Each oFile As FileInfo In oGetFile
                tPathLog &= "\" & oFile.Name
            Next


            oHDfile.Append(vbCrLf + "Success:(" & tExpSuccess & ") Fail:(" & tExpFail & ")")
            oHDfile.Append(Environment.NewLine())
            'Using oStreamWriter As StreamWriter = File.AppendText(tPathLog)
            '    With oStreamWriter
            '        .WriteLine(oHDfile)
            '        .Flush()
            '        .Close()
            '    End With
            'End Using

            Dim tName As String = Path.GetFileName(tPathLog)
            Dim tTemp As String = Path.GetTempFileName()
            Using oStreamWriter As StreamWriter = New StreamWriter(tTemp)
                Using oReader As StreamReader = New StreamReader(tPathLog)
                    oStreamWriter.Write(oHDfile)
                    While (Not oReader.EndOfStream)
                        oStreamWriter.WriteLine(oReader.ReadLine())
                    End While
                End Using

            End Using
            File.Copy(tTemp, tPathLog, True)

            W_SETxMoveLog(tPathLog)

            cExportSale.nC_Success = 0
            cExportSale.nC_Fail = 0

        Catch ex As Exception

        End Try
    End Sub


    ''' <summary>
    ''' ย้ายไฟล์ที่ Process มาไว้ที่ Log
    ''' </summary>
    Public Sub W_SETxMoveLog(ByVal ptPathName As String)
        Try
            Dim tPathName As String = Path.GetFileName(ptPathName)
            FileCopy(ptPathName, AdaConfig.cConfig.tAppPath & "\Log\" & tPathName)
            If File.Exists(ptPathName) Then
                File.Delete(ptPathName)
            End If
        Catch ex As Exception

        End Try
    End Sub


End Class