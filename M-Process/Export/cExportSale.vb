﻿Imports System.Text
Imports Newtonsoft.Json
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Collections.Generic
Imports System.Net
Imports System.Threading.Tasks
Imports System.Web.Script.Serialization
Imports System.IO
Imports System

Public Class cExportSale

    Public Shared nC_CreateFile As Integer = 0
    Public Shared nC_Success As Integer
    Public Shared nC_Fail As Integer
    Public Shared tC_DocNoProcess As String
    Public Shared oC_DbtblLog As DataTable  'Table สำหรับเก็บข้อมูล Log

    'Public Class Extensions
    '    Function AsJson(ByVal o As Object) As StringContent
    '        Return New StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json")
    '    End Function
    'End Class


    ''' <summary>
    '''    Select query sale การขาย
    ''' </summary>
    ''' <param name="ptDateDoc"> วันที่ส่งออก </param>
    ''' <param name="ptDoc">เลขที่ เอกสาร </param>
    ''' <returns></returns>
    Public Function C_EXPbExportSale(ByVal ptDateDoc As String, ByVal ptDoc As String, ByVal ptExpType As String) As Boolean
        Dim bStatus As Boolean = False

        Try
            Dim oExpSale As New cmlExpSale
            Dim oDbtblSum As New DataTable
            Dim oDbtbl As New DataTable
            Dim oDbtblDT As New DataTable
            Dim oSQL As New StringBuilder
            Dim oDatabase As New cDatabaseLocal
            '   Dim oListData As New List(Of String())
            Dim tConditions As String = ""

            Select Case ptExpType
                Case "Sale"
                    tConditions += "1"
                Case "Return"
                    tConditions += "9"
                Case "ReturnAll"
                    tConditions += "9"
            End Select

            'เตรียมข้อมูลของ Header 
            oSQL.Clear()
            oSQL.AppendLine(" SELECT SHD.FTShdDocNo, ISNULL(SHD.FTShdPosCN,'') AS FTShdPosCN , SHD.FDShdDocDate, ")
            oSQL.AppendLine(" SHD.FTCstCode, SHD.FTSpnCode, SHD.FCShdDis, SHD.FCShdGrand, SRC.FCSrcCardChg, ")
            oSQL.AppendLine(" (SELECT   FTUrsValue ")
            oSQL.AppendLine(" FROM      dbo.TLNKMappingBranch AS MAP")
            oSQL.AppendLine(" WHERE     (FTBchCode = SHD.FTBchCode)) AS FTUrsValue,")
            oSQL.AppendLine(" (SELECT   FTSpnName ")
            oSQL.AppendLine(" FROM      dbo.TCNMSpn AS SPN")
            oSQL.AppendLine(" WHERE     (FTSpnCode = SHD.FTSpnCode)) AS FTSpnName,")
            oSQL.AppendLine(" (SELECT   FTPayUrsValue ")
            oSQL.AppendLine(" FROM      dbo.TLNKMappingPayment AS PAY")
            oSQL.AppendLine(" WHERE     (FTRcvCode = SRC.FTRcvCode)) AS FTRcvCode, ")
            oSQL.AppendLine(" (SELECT    FTUsrName")
            oSQL.AppendLine(" FROM       dbo.TSysUser AS USR")
            oSQL.AppendLine(" WHERE      (FTUsrCode = SHD.FTUsrCode)) AS FTUsrName, ")
            oSQL.AppendLine(" (SELECT    FTDptName ")
            oSQL.AppendLine(" FROM       dbo.TCNMDepart AS DPM ")
            oSQL.AppendLine(" WHERE      (FTDptCode = SHD.FTDptCode)) AS FTDptName, ")
            oSQL.AppendLine(" CST.FTCstName AS FTShdCstName , CST.FTCstAddr AS FTShdCstAddr , CST.FTCstTaxNo AS FTTaxNo ")
            oSQL.AppendLine(" FROM       dbo.TPSTSalHD AS SHD INNER JOIN ")
            oSQL.AppendLine(" dbo.TPSTSalRC AS SRC ON SHD.FTShdDocNo = SRC.FTShdDocNo LEFT OUTER JOIN ")
            oSQL.AppendLine(" dbo.TCNMCst AS CST ON SHD.FTCstCode = CST.FTCstCode ")
            oSQL.AppendLine(" WHERE ((CONVERT(VARCHAR, SHD.FDShdDocDate, 103) = '" & ptDateDoc & "') AND (SHD.FTShdDocNo = '" & ptDoc & "' )) ")
            oSQL.AppendLine(" AND (SHD.FTShdDocType = " + tConditions + ")")

            oDbtbl = oDatabase.C_GEToTblSQL(oSQL.ToString())

            'เตรียมข้อมูลของ Detail
            oSQL.Clear()
            oSQL.AppendLine(" Select FTPdtCode ,FTPdtName ,FCSdtQtyAll ")
            oSQL.AppendLine(" FROM TPSTSalDT ")
            oSQL.AppendLine(" WHERE (FTShdDocNo ='" & ptDoc & "' )")

            oDbtblDT = oDatabase.C_GEToTblSQL(oSQL.ToString())
            If oDbtbl.Rows.Count > 0 Then
                C_POStSendApi(oDbtbl, oDbtblDT, ptExpType)
            Else

            End If

            bStatus = True
            Return bStatus
        Catch ex As Exception
            Return bStatus
        End Try

    End Function


    ''' <summary>
    ''' Call Api
    ''' </summary>
    ''' <param name="poDbtbl"> Datatable ของส่วน Header นำมาดึงข้อมูลเพื่อ แปลงเป็น Json และส่งไปยัง API </param>
    ''' <param name="poDbtblDT"> Datatable ของส่วน Detail นำมาดึงข้อมูลเพื่อ แปลงเป็น Json และส่งไปยัง API  </param>
    ''' <returns></returns>
    Public Function C_POStSendApi(ByVal poDbtbl As DataTable, ByVal poDbtblDT As DataTable, ByVal _
                                  ptExptype As String) As String

        Try
            Dim oJson
            Dim oRespons As New cmlRes   'สำหรับ รับค่าที่ Respons กลับมาจาก API
            Dim oExpSale As New cmlExpSale   'Model ของ ขาย ส่วน Header
            Dim oExpSaleDT As cmlExpSalDT 'Model ของขาย ส่วน Detail 
            Dim oListSaleDT As New List(Of cmlExpSalDT)  ' Model List ของ ขาย
            Dim oExpReturnBll As New cmlExpReturnBill  'Model ของ คืนทั้งบิล
            Dim oExpReturn As New cmlExpReturn
            Dim oExpReturnDT As New cmlReturnDT
            Dim oListReturnDT As New List(Of cmlReturnDT)
            Dim oResponse As New HttpResponseMessage
            Dim tStatusDesc As String = ""
            Dim tUrl As String = ""
            Dim tCodeStatus As String = ""

            Dim tAppkey As String = AdaConfig.cConfig.oConfigAPI.tAppKey  'Token Key for send API
            Select Case ptExptype
                Case "Sale"
                    tUrl = AdaConfig.cConfig.oConfigAPI.tUrlSale
                    For nRowDT As Integer = 0 To poDbtblDT.Rows.Count - 1
                        ' Dim oExpSaleDT As New cmlExpSalDT
                        oExpSaleDT = New cmlExpSalDT
                        oExpSaleDT.tItemCode = C_SETtDbnull(poDbtblDT.Rows(nRowDT)("FTPdtCode"))
                        oExpSaleDT.tItemName = C_SETtDbnull(poDbtblDT.Rows(nRowDT)("FTPdtName"))
                        oExpSaleDT.tPrice = C_CALtSumNet(poDbtbl.Rows(0)("FTShdDocNo"), poDbtblDT.Rows(nRowDT)("FTPdtCode"))
                        oExpSaleDT.tQty = C_SETtDbnull(poDbtblDT.Rows(nRowDT)("FCSdtQtyAll"))
                        oListSaleDT.Add(oExpSaleDT)
                    Next

                    'ข้อมูลส่วน Header 
                    With oExpSale

                        .tDocNo = C_SETtDbnull(poDbtbl.Rows(0)("FTShdDocNo"))
                        .tDocDate = C_SETtDbnull(Format(poDbtbl.Rows(0)("FDShdDocDate"), "yyyy-MM-dd"))
                        .tSalePoint = C_SETtDbnull(poDbtbl.Rows(0)("FTUrsValue"))
                        .tSaleCode = C_SETtDbnull(poDbtbl.Rows(0)("FTSpnCode"))
                        .tSaleName = C_SETtDbnull(poDbtbl.Rows(0)("FTSpnName"))
                        .tCustomerCode = C_SETtDbnull(poDbtbl.Rows(0)("FTCstCode"))
                        .tCustomerName = C_SETtDbnull(poDbtbl.Rows(0)("FTShdCstName"))
                        .tCustomerAddress = C_SETtDbnull(poDbtbl.Rows(0)("FTShdCstAddr"))
                        .tCustomerTaxId = C_SETtDbnull(poDbtbl.Rows(0)("FTTaxNo"))
                        .tPaymentType = C_SETtDbnull(poDbtbl.Rows(0)("FTRcvCode"))
                        .tReceiveBy = C_SETtDbnull(poDbtbl.Rows(0)("FTUsrName"))
                        .tReceiveByPosition = C_SETtDbnull(poDbtbl.Rows(0)("FTDptName"))
                        .tDiscount = C_SETtDbnull(poDbtbl.Rows(0)("FCShdDis"))
                        .tCreditFee = C_SETtDbnull(poDbtbl.Rows(0)("FCSrcCardChg"))
                        .tTotal = C_SETtDbnull(poDbtbl.Rows(0)("FCShdGrand"))
                        .oItem = oListSaleDT

                    End With
                    oJson = JsonConvert.SerializeObject(oExpSale)
                Case "Return"
                    tUrl = AdaConfig.cConfig.oConfigAPI.tUrlReturn
                    For nRow As Integer = 0 To poDbtblDT.Rows.Count - 1
                        oExpReturnDT = New cmlReturnDT
                        oExpReturnDT.itemCode = C_SETtDbnull(poDbtblDT.Rows(nRow)("FTPdtCode"))
                        oListReturnDT.Add(oExpReturnDT)
                    Next

                    oExpReturn.docNo = C_SETtDbnull(poDbtbl.Rows(0)("FTShdPosCN"))
                    oExpReturn.item = oListReturnDT
                    oJson = JsonConvert.SerializeObject(oExpReturn)
                Case "ReturnAll"
                    tUrl = AdaConfig.cConfig.oConfigAPI.tUrlReturnAll
                    With oExpReturnBll
                        .docNo = C_SETtDbnull(poDbtbl.Rows(0)("FTShdPosCN"))
                    End With
                    oJson = JsonConvert.SerializeObject(oExpReturnBll)

            End Select

            '   With oExpSaleDT

            'ข้อมูลส่วน Detail

            '    End With
            '   Dim oJson =
            Dim oStringContent = New StringContent(oJson, UnicodeEncoding.UTF8, "application/json")
            Dim oClient = New HttpClient()

            '== ส่ง Token Key ไปใน Header =='
            oClient.DefaultRequestHeaders.Clear()
            oClient.DefaultRequestHeaders.Add("Application-Key", tAppkey)

            oResponse = oClient.PostAsync(tUrl, oStringContent).Result
            If oResponse.IsSuccessStatusCode Then

                Dim oStream As HttpContent = oResponse.Content
                Dim oData = oStream.ReadAsStringAsync().Result
                oRespons = (New JavaScriptSerializer()).Deserialize(Of cmlRes)(oData)
                tStatusDesc = oRespons.StatusDesc
                tCodeStatus = oRespons.StatusCode

                If tCodeStatus = "ST0000" Then
                    nC_CreateFile += 1
                    nC_Success += 1
                    cLog.C_CALxWriteLogExport(tStatusDesc, tCodeStatus, poDbtbl.Rows(0).Item("FTShdDocNo"), ptExptype, poDbtbl.Rows(0)("FTShdPosCN"))
                    'oC_DbtblLog = W_SETtColumnLog()
                    'Dim oDatarow As DataRow
                    'oDatarow = oC_DbtblLog.NewRow()
                    'oDatarow("FTStatus") = tStatus
                    'oDatarow("FTShdDocNo") = poDbtbl.Rows(0).Item("FTShdDocNo")
                    'oDatarow("FTExptype") = ptExptype
                    'oDatarow("FTShdPosCN") = poDbtbl.Rows(0)("FTShdPosCN")
                    'oC_DbtblLog.Rows.Add(oDatarow)
                    'C_SETxWriteTextFile(poDbtbl, poDbtblDT, ptExptype)
                    C_SETxWriteJson(oJson, ptExptype, poDbtbl, poDbtblDT)
                Else
                    nC_CreateFile += 1
                    nC_Fail += 1
                    'C_SETxWriteTextFile(poDbtbl, poDbtblDT, ptExptype)
                    C_SETxWriteJson(oJson, ptExptype, poDbtbl, poDbtblDT)
                    cLog.C_CALxWriteLogExport(tStatusDesc, tCodeStatus, poDbtbl.Rows(0).Item("FTShdDocNo"), ptExptype, poDbtbl.Rows(0)("FTShdPosCN"))
                End If
            End If

            Return tStatusDesc
        Catch ex As Exception
            nC_Fail += 1
            Throw
        End Try

    End Function


    ''' <summary>
    ''' คำนวณ ราคา
    ''' </summary>
    ''' <param name="tDoc"> เลขที่เอกสาร </param>
    ''' <param name="tPdtCode">รหัสสินค้า</param>
    ''' <returns></returns>
    Public Function C_CALtSumNet(ByVal tDoc As String, ByVal tPdtCode As String) As String
        Dim tpdtSum As String = ""
        Try

            Dim oSql As New StringBuilder
            Dim oDbtblSum As New DataTable
            Dim oDatabase As New cDatabaseLocal

            oSql.Clear()
            oSql.AppendLine(" SELECT SUM(FCSdtNet - (FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg)) AS FTResult ")
            oSql.AppendLine(" FROM TPSTSalDT ")
            oSql.AppendLine(" WHERE (FTShdDocNo = '" & tDoc & "') AND (FTPdtCode = '" & tPdtCode & "') ")
            oDbtblSum = oDatabase.C_GEToTblSQL(oSql.ToString())

            tpdtSum = oDbtblSum.Rows(0)("FTResult")

        Catch ex As Exception
            Return ""
        End Try
        Return tpdtSum
    End Function

    ''' <summary>
    ''' Write textfile
    ''' </summary>
    ''' <param name="poDataHD"> ข้อมูลตาราง Header</param>
    ''' <param name="poDataDT"> ข้อมูลตาราง Detail</param>

    Public Sub C_SETxWriteTextFile(ByVal poDataHD As DataTable, ByVal poDataDT As DataTable, ByVal ptStatus As String)
        Dim tPathFile As String = ""
        Dim oWrite As New StringBuilder
        Dim oExpSale As New cmlExpSale

        Try
            Dim tPathBackup As String = AdaConfig.cConfig.tAppPath & "\Backup"  'เพิ่ม Folder AdaLog *CH 02-12-2014 

            If Not IO.Directory.Exists(tPathBackup) Then
                IO.Directory.CreateDirectory(tPathBackup)
            End If

            tPathBackup &= "\" & "ExportSale_" & Format(Date.Now, "yyyyMMddhhmm") & "-" & poDataHD.Rows(0)("FTShdDocNo") & ".txt"

            If File.Exists(tPathBackup) = False Then
                Using oSw As StreamWriter = File.CreateText(tPathBackup)
                    oSw.Close()
                End Using
            End If

            Select Case ptStatus
                Case "Sale"
                    oWrite.Append("{" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "DocNo" + ":" + """" + poDataHD.Rows(0)("FTShdDocNo") + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "docDate" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FDShdDocDate")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "salePoint" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FTUrsValue")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "saleCode" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FTSpnCode")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "saleName" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FTSpnName")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "customerCode" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FTCstCode")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "customerName" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FTShdCstName")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "customerAddress" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FTShdCstAddr")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "customerTaxId" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FTTaxNo")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "paymentType" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FTRcvCode")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "receiveBy" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FTUsrName")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "receiveByPosition" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FTDptName")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "discount" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FCShdDis")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "creditFee" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FCSrcCardChg")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "total" + ":" + """" + C_SETtDbnull(poDataHD.Rows(0)("FCShdGrand")) + """" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "item" + Environment.NewLine)
                    oWrite.Append("[" + Environment.NewLine)

                    For nRowDT As Integer = 0 To poDataDT.Rows.Count - 1
                        oWrite.Append("{" + Environment.NewLine)
                        oWrite.Append(vbTab + vbTab + vbTab + "itemCode :" + """" + poDataDT.Rows(nRowDT)("FTPdtCode") + """" + Environment.NewLine)
                        oWrite.Append(vbTab + vbTab + vbTab + "itemName :" + """" + poDataDT.Rows(nRowDT)("FTPdtName") + """" + Environment.NewLine)
                        oWrite.Append(vbTab + vbTab + vbTab + "price :" + """" + C_CALtSumNet(poDataHD.Rows(0)("FTShdDocNo"), poDataDT.Rows(nRowDT)("FTPdtCode")) + """" + Environment.NewLine)
                        oWrite.Append(vbTab + vbTab + vbTab + "qty :" + """" + C_SETtDbnull(poDataDT.Rows(nRowDT)("FCSdtQtyAll")) + """" + Environment.NewLine)
                        oWrite.Append("}" + Environment.NewLine)
                    Next

                    oWrite.Append("]" + Environment.NewLine)
                    oWrite.Append("}" + Environment.NewLine)

                Case "Return"
                    oWrite.Append("{" + Environment.NewLine)
                    oWrite.Append(vbTab + vbTab + "DocNo" + ":" + """" + poDataHD.Rows(0)("FTShdPosCN") + """" + Environment.NewLine)
                    oWrite.Append("item" + Environment.NewLine)
                    oWrite.Append("}")

                    For nRowDT As Integer = 0 To poDataDT.Rows.Count - 1
                        oWrite.Append("{" + Environment.NewLine)
                        oWrite.Append(vbTab + vbTab + "itemCode :" + """" + poDataDT.Rows(nRowDT)("FTPdtCode") + """" + Environment.NewLine)
                        oWrite.Append(vbTab + vbTab + "itemName :" + """" + poDataDT.Rows(nRowDT)("FTPdtName") + """" + Environment.NewLine)
                        oWrite.Append(vbTab + vbTab + "price :" + """" + C_CALtSumNet(poDataHD.Rows(0)("FTShdDocNo"), poDataDT.Rows(nRowDT)("FTPdtCode")) + """" + Environment.NewLine)
                        oWrite.Append(vbTab + vbTab + "qty :" + """" + C_SETtDbnull(poDataDT.Rows(nRowDT)("FCSdtQtyAll")) + """" + Environment.NewLine)
                        oWrite.Append("}" + Environment.NewLine)
                    Next

                Case "ReturnAll"
                    oWrite.Append("{" + Environment.NewLine)
                    oWrite.Append(vbTab + "DocNo" + ":" + """" + poDataHD.Rows(0)("FTShdPosCN") + """" + Environment.NewLine)
                    oWrite.Append("}")

            End Select


            Using oStreamWriter As StreamWriter = File.AppendText(tPathBackup)
                With oStreamWriter
                    .WriteLine(oWrite)
                    .Flush()
                    .Close()
                End With
            End Using


        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Convert to string 
    ''' </summary>
    ''' <param name="oVal">Value </param>
    ''' <returns></returns>
    Public Function C_SETtDbnull(ByVal oVal As Object) As String

        Try
            If oVal Is DBNull.Value Then
                Return ""
            Else
                Return oVal
            End If
        Catch ex As Exception

        End Try
    End Function

    Public Function C_SETtColumnLog() As DataTable
        Try

            Dim oDbtbl As New DataTable()
            oDbtbl.Columns.Add("FTStatus", GetType(String))
            oDbtbl.Columns.Add("FTShdDocNo", GetType(String))
            oDbtbl.Columns.Add("FTExptype", GetType(String))
            oDbtbl.Columns.Add("FTShdPosCN", GetType(String))

            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Sub C_SETxWriteJson(ByVal poDataJson As Object, ByVal ptStatus As String, ByVal poDataHD As DataTable, ByVal poDataDT As DataTable)
        Try

            Dim tPathFile As String = ""
            Dim oWrite As New StringBuilder
            Dim oExpSale As New cmlExpSale

            Try
                Dim tPathBackup As String = AdaConfig.cConfig.tAppPath & "\Backup"  'เพิ่ม Folder AdaLog *CH 02-12-2014 

                If Not IO.Directory.Exists(tPathBackup) Then
                    IO.Directory.CreateDirectory(tPathBackup)
                End If

                tPathBackup &= "\" & "ExportSale_" & Format(Date.Now, "yyyyMMddhhmm") & "-" & poDataHD.Rows(0)("FTShdDocNo") & ".txt"

                If File.Exists(tPathBackup) = False Then
                    Using oSw As StreamWriter = File.CreateText(tPathBackup)
                        oSw.Close()
                    End Using
                End If

                Select Case ptStatus
                    Case "Sale"
                        oWrite.Append(poDataJson)
                    Case "Return"
                        oWrite.Append(poDataJson)
                    Case "ReturnAll"
                        oWrite.Append(poDataJson)
                End Select


                Using oStreamWriter As StreamWriter = File.AppendText(tPathBackup)
                    With oStreamWriter
                        .WriteLine(oWrite)
                        .Flush()
                        .Close()
                    End With
                End Using


            Catch ex As Exception

            End Try

        Catch ex As Exception

        End Try
    End Sub




End Class
