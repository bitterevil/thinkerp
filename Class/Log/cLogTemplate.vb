﻿Imports System.Data.SqlClient

Public Class cLogTemplate

    Public Shared Property tC_FileMaster As String = AdaConfig.cConfig.oConfigXml.tLog & "\Log_{0}"
    Public Shared Property tC_FileTCNTPdtAJP As String = AdaConfig.cConfig.oConfigXml.tLog & "\Log_{0}"
    Public Shared Property tC_FileTCNTPdtTnf As String = AdaConfig.cConfig.oConfigXml.tLog & "\Log_{0}"
    Public Shared Property tC_FileTCNMSpl As String = AdaConfig.cConfig.oConfigXml.tLog & "\Log_{0}"
    Public Shared Property tC_FileTCNTPmt As String = AdaConfig.cConfig.oConfigXml.tLog & "\Log_{0}"
    Public Shared Property tC_FileTCNMPdtBrand As String = AdaConfig.cConfig.oConfigXml.tLog & "\Log_{0}"

    'List Log Import แสดงใน Grid
    Private Shared oC_LogImports As List(Of cLogDetail)
    Public Shared Function C_GETaLogImport(Optional ptWhereDate As String = "") As List(Of cLogDetail)
        oC_LogImports = New List(Of cLogDetail)
        If ptWhereDate.Length > 0 Then
            ptWhereDate = "CONVERT(VARCHAR(10),FDDateTime,120)='" & ptWhereDate & "'"
        Else
            ptWhereDate = "1=1"
        End If

        With oC_LogImports
            Dim tSql As String = "SELECT FDDateTime, FTFileName, FTDescription, FTRefer FROM TLNKLog WHERE FNType=1 AND " & ptWhereDate & " ORDER BY FDDateTime DESC"
            Using oSQLConn As New SqlClient.SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSQLConn.Open()
                Dim oSQLCmd As New SqlClient.SqlCommand(tSql, oSQLConn)
                Dim oSQLRead As SqlClient.SqlDataReader = oSQLCmd.ExecuteReader()
                While oSQLRead.Read()
                    .Add(New cLogDetail With {.tDate = Format(oSQLRead(0), "dd/MM/yyyy"), .tTime = Format(oSQLRead(0), "HH:mm:ss"), .tFileName = oSQLRead(1), .tDesc = oSQLRead(2), .tRefer = oSQLRead(3)})
                End While
            End Using
        End With
        Return oC_LogImports
    End Function

    'List Log Export แสดงใน Grid
    Private Shared oC_LogExports As List(Of cLogDetail)
    Public Shared Function C_GETaLogExport(Optional ptWhereDate As String = "") As List(Of cLogDetail)
        oC_LogExports = New List(Of cLogDetail)
        If ptWhereDate.Length > 0 Then
            ptWhereDate = "CONVERT(VARCHAR(10),FDDateTime,120)='" & ptWhereDate & "'"
        Else
            ptWhereDate = "1=1"
        End If

        With oC_LogExports
            Dim tSql As String = "SELECT FDDateTime, FTFileName, FTDescription, FTRefer FROM TLNKLog WHERE FNType=2 AND " & ptWhereDate & " ORDER BY FDDateTime DESC"
            Using oSQLConn As New SqlClient.SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
                oSQLConn.Open()
                Dim oSQLCmd As New SqlClient.SqlCommand(tSql, oSQLConn)
                Dim oSQLRead As SqlClient.SqlDataReader = oSQLCmd.ExecuteReader()
                While oSQLRead.Read()
                    .Add(New cLogDetail With {.tDate = Format(oSQLRead(0), "dd/MM/yyyy"), .tTime = Format(oSQLRead(0), "HH:mm:ss"), .tFileName = oSQLRead(1), .tDesc = oSQLRead(2), .tRefer = oSQLRead(3)})
                End While
            End Using
        End With
        Return oC_LogExports
    End Function

    'Delete Log
    Public Shared Sub C_CALxDeleteLog(ptDateWhere As String)
        Dim tSql As String = "DELETE FROM TLNKLog WHERE " & ptDateWhere
        Using oConnSQL As New SqlConnection(AdaConfig.cConfig.tSQLConnSourceString)
            oConnSQL.Open()
            Using oSQLCmd As New SqlCommand
                Try
                    oSQLCmd.Connection = oConnSQL
                    oSQLCmd.CommandTimeout = AdaConfig.cConfig.oConnSource.nTimeOut
                    oSQLCmd.CommandText = tSql
                    oSQLCmd.ExecuteNonQuery()
                Catch ex As Exception
                End Try
            End Using
        End Using
    End Sub

    'Sub Class Detail
    Class cLogDetail
        Property tDate As String = ""
        Property tTime As String = ""
        Property tFileName As String = ""
        Property tDesc As String = ""
        Property tRefer As String = ""
    End Class

End Class


