﻿Imports System.IO

Public Class cImportLog

    Private Const tC_FmtLineHead As String = "[Import Date] {0}" & vbTab & "[Import Time] {1}" & vbTab & "[Import By] {2}"
    Private Const tC_FmtLineColumn As String = "[LineNo.]" & vbTab & "[Type]" & vbTab & "[DocNo.]" & vbTab & "[Seq.]" & vbTab & "[Code]" & vbTab & "[Name]" & vbTab & "[ErrorField]" & vbTab & "[ErrorDescription]"
    Private Const tC_FmtLineData As String = "{0}" & vbTab & "{1}" & vbTab & "{2}" & vbTab & "{3}" & vbTab & "{4}" & vbTab & "{5}" & vbTab & "{6}" & vbTab & "{7}"
    Private Const tC_FmtLineColumn2 As String = "[LineNo.]" & vbTab & "[FileName]" & vbTab & "[DocNo.]" & vbTab & "[Name]" & vbTab & "[ErrorField]" & vbTab & "[ErrorDescription]"
    Private Const tC_FmtLineData2 As String = "{0}" & vbTab & "{1}" & vbTab & "{2}" & vbTab & "{3}" & vbTab & "{4}" & vbTab & "{5}"
    Private Const tC_FmtLine As String = "====================================================================================================================================="
    Private tC_Date As String = Format(Date.Now, "dd/MM/yyyy")
    Private tC_Time As String = Format(Date.Now, "HH:mm:ss")
    Private tC_ComName As String = My.Computer.Name
    Private tC_FileName As String = ""
    Private aC_ListLog As List(Of cLog)

    Sub New(ptFileNameLog As String)
        tC_FileName = ptFileNameLog
    End Sub

    Public Sub C_CALxAddNew(poLog As cLog)
        If aC_ListLog Is Nothing Then
            aC_ListLog = New List(Of cLog)
        End If
        aC_ListLog.Add(poLog)
    End Sub

    ''' <summary>
    ''' Save Log File
    ''' </summary>
    ''' <param name="ptColumn">Version Show Column</param>
    ''' <returns>Boolean</returns>
    Public Function C_CALbSave(Optional ByVal ptColumn As String = "") As Boolean
        C_CALbSave = False
        Try
            'Count จำนวน Error มากว่า 0 ถึงจะสร้าง File Log
            If aC_ListLog Is Nothing Then
                Return True
            End If

            If aC_ListLog.Count > 0 Then
                Dim bHDFile As Boolean = False
                If File.Exists(tC_FileName) = False Then
                    Using oStreamWriter As StreamWriter = File.CreateText(tC_FileName)
                        oStreamWriter.Close()
                    End Using
                    bHDFile = True
                End If

                Using oStreamWriter As StreamWriter = File.AppendText(tC_FileName)
                    With oStreamWriter
                        'If bHDFile = True Then
                        .WriteLine(tC_FmtLine)
                        .WriteLine(String.Format(tC_FmtLineHead, tC_Date, tC_Time, tC_ComName))
                        .WriteLine(tC_FmtLine)
                        '.WriteLine(tW_FmtLine)
                        'Select Case ptColumn
                        '    Case ""
                        '        .WriteLine(tC_FmtLineColumn)
                        '    Case "2"
                        '        .WriteLine(tC_FmtLineColumn2)
                        'End Select
                        ' .WriteLine(tW_FmtLine)
                        .Flush()
                        'End If

                        For Each oLog In aC_ListLog '.OrderBy(Function(c) c.nLineNo)
                            With oLog
                                Select Case ptColumn
                                    Case ""
                                        oStreamWriter.WriteLine(String.Format(tC_FmtLineData, .nLineNo, .tType, .tDocNo, .tSeqNo, .tCode, .tName, .tErrField, cLogMsg.C_GETtMsg(.tErrField) & .tErrDesc))
                                    Case "2"
                                        oStreamWriter.WriteLine(String.Format(tC_FmtLineData2, .nLineNo, .tType, .tDocNo, .tName, .tErrField, .tErrDesc))
                                End Select
                            End With
                            .Flush()
                        Next
                        oStreamWriter.WriteLine("")
                        .Close()
                    End With
                End Using

            End If
            C_CALbSave = True
        Catch ex As Exception
        End Try
    End Function

    'Sub Class Log
    Class cLog
        Property nLineNo As Integer = 0
        Property tType As String = ""
        Property tDocNo As String = ""
        Property tSeqNo As String = ""
        Property tCode As String = ""
        Property tName As String = ""
        Property tErrField As String = ""
        Property tErrDesc As String = ""
    End Class

End Class

Public Class cImportLogHis

    Private Const tC_FmtLineColumn As String = "[DocDate]" & vbTab & "[Product code]" & vbTab & "[Stock code]" & vbTab & "[Barcode]"
    Private Const tC_FmtLineData As String = "{0}" & vbTab & "{1}" & vbTab & "{2}" & vbTab & "{3}"
    Private Const tC_FmtErrHead As String = "[Import Date] {0}" & vbTab & "[Import Time] {1}" & vbTab & "[Import By] {2}"
    Private Const tC_FmtErrColumn As String = "[ErrorDescription]"
    Private Const tC_FmtErrData As String = "{0}"
    Private Const tC_FmtLine As String = "====================================================================================================================================="
    Private tC_Date As String = Format(Date.Now, "dd/MM/yyyy")
    Private tC_Time As String = Format(Date.Now, "HH:mm:ss")
    Private tC_ComName As String = My.Computer.Name
    Private tC_FileName As String = ""
    Private aC_ListLog As List(Of cLogHis)

    Sub New(ptFileNameLog As String)
        tC_FileName = ptFileNameLog
    End Sub

    Public Sub C_CALxAddNew(poLog As cLogHis)
        If aC_ListLog Is Nothing Then
            aC_ListLog = New List(Of cLogHis)
        End If
        aC_ListLog.Add(poLog)
    End Sub

    Public Function C_CALbSave(ByVal pbShwCol As Boolean) As Boolean
        C_CALbSave = False
        Try
            'Count จำนวน Error มากว่า 0 ถึงจะสร้าง File Log
            If aC_ListLog Is Nothing Then
                Return True
            End If

            If aC_ListLog.Count > 0 Then
                Dim bHDFile As Boolean = False
                If File.Exists(tC_FileName) = False Then
                    Using oStreamWriter As StreamWriter = File.CreateText(tC_FileName)
                        oStreamWriter.Close()
                    End Using
                    bHDFile = True
                End If

                Using oStreamWriter As StreamWriter = File.AppendText(tC_FileName)
                    With oStreamWriter
                        '.WriteLine(tW_FmtLine)
                        If pbShwCol Then .WriteLine(tC_FmtLineColumn)
                        ' .WriteLine(tW_FmtLine)
                        .Flush()
                        'End If

                        For Each oLog In aC_ListLog '.OrderBy(Function(c) c.nLineNo)
                            With oLog
                                oStreamWriter.WriteLine(String.Format(tC_FmtLineData, .tDocDate, .tPdtCode, .tStkCode, .tBarCode))
                            End With
                            .Flush()
                        Next
                        .Close()
                    End With
                End Using

            End If
            C_CALbSave = True
        Catch ex As Exception
        End Try
    End Function

    Public Function C_CALbSaveLogErr() As Boolean
        C_CALbSaveLogErr = False
        Try
            'Count จำนวน Error มากว่า 0 ถึงจะสร้าง File Log
            If aC_ListLog Is Nothing Then
                Return True
            End If

            If aC_ListLog.Count > 0 Then
                Dim bHDFile As Boolean = False
                If File.Exists(tC_FileName) = False Then
                    Using oStreamWriter As StreamWriter = File.CreateText(tC_FileName)
                        oStreamWriter.Close()
                    End Using
                    bHDFile = True
                End If

                Using oStreamWriter As StreamWriter = File.AppendText(tC_FileName)
                    With oStreamWriter
                        .WriteLine(tC_FmtLine)
                        .WriteLine(String.Format(tC_FmtErrHead, tC_Date, tC_Time, tC_ComName))
                        .WriteLine(tC_FmtLine)
                        .WriteLine(tC_FmtErrColumn)
                        .Flush()
                        'End If

                        For Each oLog In aC_ListLog '.OrderBy(Function(c) c.nLineNo)
                            With oLog
                                oStreamWriter.WriteLine(String.Format(tC_FmtErrData, .tErrMsg))
                            End With
                            .Flush()
                        Next
                        .Close()
                    End With
                End Using

            End If
            C_CALbSaveLogErr = True
        Catch ex As Exception
        End Try
    End Function

    'Sub Class Log
    Class cLogHis
        Property tDocDate As String = ""
        Property tPdtCode As String = ""
        Property tStkCode As String = ""
        Property tBarCode As String = ""
        Property tErrMsg As String = ""
    End Class

End Class
