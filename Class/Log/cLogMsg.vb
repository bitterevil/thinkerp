﻿Public Class cLogMsg

    Private Shared aC_Msg As Dictionary(Of String, String)

    Private Shared Sub C_SETxMsg()
        aC_Msg = New Dictionary(Of String, String)
        Try
            'TCNMPdt
            aC_Msg.Add("FTPdtCode", "รหัสสินค้า")
            aC_Msg.Add("FTPdtName", "ชื่อสินค้า")
            aC_Msg.Add("FTPdtNameOth", "ชื่ออื่น")
            aC_Msg.Add("FTPdtBarCode1", "รหัสบาร์โค้ด1")
            aC_Msg.Add("FTPgpChain", "รหัสกลุ่มสินค้า")
            aC_Msg.Add("FTPdtVatType", "ภาษีซื้อ")
            aC_Msg.Add("FTPdtSaleType", "ใช้ราคาขาย")
            aC_Msg.Add("FTPdtSUnit", "รหัสหน่วย")
            aC_Msg.Add("FCPdtRetPriS1", "ราคาขายปลีก1")
            aC_Msg.Add("FTPdtType", "ประเภทสินค้า")
            aC_Msg.Add("FTPdtRmk", "หมายเหตุ(รหัสกลุ่มของTplus)")
            aC_Msg.Add("FTPdtNoDis", "สินค้าห้ามลด")
            'TCNMPdtType
            aC_Msg.Add("FTPtyCode", "รหัสประเภทสินค้า")
            aC_Msg.Add("FTPtyName", "ชื่อประเภทสินค้า")
            'TCNMPdtGrp
            aC_Msg.Add("FTPgpName", "ชื่อกลุ่ม")
            aC_Msg.Add("FTPgpRmk", "หมายเหตุ(รหัสกลุ่มของTplus)")
            'TCNMPdtBrand
            aC_Msg.Add("FTPbnCode", "รหัสยี่ห้อ")
            aC_Msg.Add("FTPbnName", "ชื่อยี่ห้อ")
            'TCNMBranch
            aC_Msg.Add("FTBchCode", "รหัสสาขา")
            aC_Msg.Add("FTBchName", "ชื่อสาขา")
            aC_Msg.Add("FTBchHQ", "สถานะสำนักงานใหญ่")
            'TCNTPmtHD
            aC_Msg.Add("FTPmhBchTo", "รหัสสาขาที่มีผล")
            aC_Msg.Add("FTPmhCode", "เลขที่เอกสารโปรโมชั่น")
            aC_Msg.Add("FTPmhName", "ชื่อโปรโมชั่น")
            aC_Msg.Add("FTPmhNameSlip", "ชื่อโปรโมชั่น(แบบย่อ)")
            aC_Msg.Add("FTSpmCode", "รหัสรูปแบบโปรโมชั่น")
            aC_Msg.Add("FDPmhDStart", "วันที่เริ่มโปรโมชั่น")
            aC_Msg.Add("FDPmhDStop", "วันที่สิ้นสุดโปรโมชั่น")
            aC_Msg.Add("FTPmhStatus", "สถานะโปรโมชั่น")
            aC_Msg.Add("FTPmhRmk", "หมายเหตุ")
            aC_Msg.Add("FTSpmStaRcvFree", "สถานะใช้งาน กำหนดรับของแถมที่จุดขาย")
            'TCNTPmtDT
            aC_Msg.Add("FNPmdSeq", "ลำดับ")
            aC_Msg.Add("FTPmdGrpType", "ประเภทสินค้าในกลุ่ม")
            aC_Msg.Add("FTPmdGrpName", "กลุ่มสินค้าโปรโมชั่น")
            aC_Msg.Add("FTPmdBarCode", "รหัสบาร์โค้ด")
            'TCNTPmtCD
            aC_Msg.Add("FNPmcSeq", "ลำดับ")
            aC_Msg.Add("FTPmcGrpName", "ชื่อกลุ่มจัดรายการ")
            aC_Msg.Add("FTPmcStaGrpCond", "ประเภทกลุ่ม")
            aC_Msg.Add("FCPmcPerAvgDis", "%เฉลี่ย")
            aC_Msg.Add("FCPmcBuyAmt", "ซื้อครบมูลค่า")
            aC_Msg.Add("FCPmcBuyQty", "ซื้อครบจำนวน")
            aC_Msg.Add("FCPmcBuyMinQty", "จากจำนวน")
            aC_Msg.Add("FCPmcBuyMaxQty", "ถึงจำนวน")
            aC_Msg.Add("FCPmcGetCond", "รูปแบบส่วนลด")
            aC_Msg.Add("FCPmcGetValue", "มูลค่า")
            aC_Msg.Add("FCPmcGetQty", "จำนวนที่จะได้รับ")
            'TCNTPdtAjpHD
            aC_Msg.Add("FTIphDocNo", "เลขที่เอกสารปรับราคา")
            aC_Msg.Add("FDIphDocDate", "วันที่เอกสาร")
            aC_Msg.Add("FTIphBchTo", "รหัสสาขาปลายทาง (สาขาที่มีผล)")
            aC_Msg.Add("FTIphRmk", "หมายเหตุ")
            aC_Msg.Add("FTIphAdjType", "ประเภทปรับราคา")
            aC_Msg.Add("FDIphAffect", "วันที่เอกสารมีผล")
            'TCNTPdtAjpDT
            aC_Msg.Add("FNIpdSeqNo", "ลำดับ")
            aC_Msg.Add("FTIpdBarCode", "รหัสบาร์โค้ด")
            aC_Msg.Add("FCIpdPriNew", "ราคาขายใหม่")
            'TCNTPdtTnfHD
            aC_Msg.Add("FTPthBchTo", "รหัสสาขาปลายทาง")
            aC_Msg.Add("FTPthDocNo", "เลขที่เอกสารโอนสินค้า")
            aC_Msg.Add("FDPthDocDate", "วันที่เอกสาร")
            aC_Msg.Add("FTPthWhFrm", "รหัสคลัง")
            aC_Msg.Add("FTPthWhTo", "รหัสคลัง")
            'TCNTPdtTnfDT
            aC_Msg.Add("FNPtdSeqNo", "ลำดับ")
            aC_Msg.Add("FTPtdBarCode", "รหัสบาร์โค้ด")
            aC_Msg.Add("FTPdtArticle", "สถานะห้ามขาย")
            aC_Msg.Add("FTPdtNoDis", "สินค้าห้ามลด")
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Function C_GETtMsg(ptFieldName As String) As String
        If aC_Msg Is Nothing Then
            C_SETxMsg()
        End If
        Return aC_Msg.Where(Function(c) c.Key = ptFieldName).FirstOrDefault.Value
    End Function

End Class
