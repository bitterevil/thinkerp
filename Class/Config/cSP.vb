﻿Imports System.Security.Cryptography

Namespace AdaConfig

    Public Class cCNSP

#Region " Encrypt - Decrypt "

        Private Shared TripleDes As New TripleDESCryptoServiceProvider

        Private Shared Function TruncateHash( _
        ByVal ptkey As String, _
        ByVal piLength As Integer) _
        As Byte()

            Dim sha1 As New SHA1CryptoServiceProvider

            ' Hash the key.
            Dim keyBytes() As Byte = _
                System.Text.Encoding.Unicode.GetBytes(ptkey)
            Dim hash() As Byte = sha1.ComputeHash(keyBytes)

            ' Truncate or pad the hash.
            ReDim Preserve hash(piLength - 1)
            Return hash
        End Function

        Public Shared Function SP_EncryptData( _
        ByVal ptPlaintext As String, ByVal ptkey As String) _
        As String

            ' Convert the plaintext string to a byte array.
            Dim plaintextBytes() As Byte = _
                System.Text.Encoding.Unicode.GetBytes(ptPlaintext)

            ' Initialize the crypto provider.
            TripleDes.Key = TruncateHash(ptkey, TripleDes.KeySize \ 8)
            TripleDes.IV = TruncateHash("", TripleDes.BlockSize \ 8)

            ' Create the stream.
            Dim ms As New System.IO.MemoryStream
            ' Create the encoder to write to the stream.
            Dim encStream As New CryptoStream(ms, _
                TripleDes.CreateEncryptor(), _
                System.Security.Cryptography.CryptoStreamMode.Write)

            ' Use the crypto stream to write the byte array to the stream.
            encStream.Write(plaintextBytes, 0, plaintextBytes.Length)
            encStream.FlushFinalBlock()

            ' Convert the encrypted stream to a printable string.
            Return Convert.ToBase64String(ms.ToArray)
        End Function

        Public Shared Function SP_DecryptData( _
        ByVal ptEncryptedtext As String, ByVal ptkey As String) _
        As String

            ' Convert the encrypted text string to a byte array.

            '     Dim encryptedBytes() As Byte = Convert.FromBase64String(encryptedtext)

            ' Initialize the crypto provider.
            TripleDes.Key = TruncateHash(ptkey, TripleDes.KeySize \ 8)
            TripleDes.IV = TruncateHash("", TripleDes.BlockSize \ 8)

            Dim encryptedBytes() As Byte
            Try
                encryptedBytes = Convert.FromBase64String(ptEncryptedtext)
            Catch ex As Exception
                Return "Error"
            End Try

            ' Create the stream.
            Dim ms As New System.IO.MemoryStream
            ' Create the decoder to write to the stream.
            Dim decStream As New CryptoStream(ms, _
                TripleDes.CreateDecryptor(), _
                System.Security.Cryptography.CryptoStreamMode.Write)

            ' Use the crypto stream to write the byte array to the stream.
            decStream.Write(encryptedBytes, 0, encryptedBytes.Length)
            decStream.FlushFinalBlock()

            ' Convert the plaintext stream to a string.
            Return System.Text.Encoding.Unicode.GetString(ms.ToArray)

        End Function

#End Region

        Public Shared Function SP_STRtToken(ByRef ptSource$, ByVal ptSep$, Optional ByVal piLoop% = 0, Optional ByVal pbRetOld As Boolean = False) As String
            '----------------------------------------------------------
            '   Call:   ptSource source string ("Com1;Com2")
            '               ptSep separator string (";")
            '               piLoop is optional if exactly know the position
            '               pbRetOld is optional if true source still the same, otherwise token out
            '   Ret:    first token from source string ("Com1")
            '               ptSource without first token ("Com2")  [depend on pbRet]
            '----------------------------------------------------------
            Dim nIndex As Integer = 0
            Dim nMaxLoop As Integer = 0
            Dim nLoop As Integer = 0
            Dim tRet As String = ""
            Dim tSrc As String = ""

            tSrc = ptSource
            If (piLoop = 0) Then
                nMaxLoop = 1
            Else
                nMaxLoop = piLoop
            End If

            For nLoop = 1 To nMaxLoop
                nIndex = InStr(UCase(tSrc), UCase(ptSep))
                If nIndex = 0 Then
                    tRet = tSrc            'not found separator (n could be 1 or >1)
                    tSrc = ""
                Else
                    tRet = Left(tSrc, nIndex - 1)
                    tSrc = Mid(tSrc, nIndex + Len(ptSep))
                End If
            Next nLoop

            If Not pbRetOld Then        'default pbRetOld = False, always token out
                ptSource = tSrc
            End If

            SP_STRtToken = tRet

        End Function

    End Class

    'Ext Module
    Module mCNSP

        <System.Runtime.CompilerServices.Extension()> _
        Public Function SP_EncryptData(ByVal ptValue As String) As String
            If ptValue.Length = 0 Then Return ""
            SP_EncryptData = cCNSP.SP_EncryptData(ptValue, cCNCS.tCS_CNEncDec)
        End Function

        <System.Runtime.CompilerServices.Extension()> _
        Public Function SP_DecryptData(ByVal ptValue As String) As String
            If ptValue.Length = 0 Then Return ""
            SP_DecryptData = cCNSP.SP_DecryptData(ptValue, cCNCS.tCS_CNEncDec)
        End Function

    End Module

End Namespace



