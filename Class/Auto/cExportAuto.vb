﻿Imports System.Text
Public Class cExportAuto

    Public Sub C_CALxProcess()

        Try
            W_GENxCrtMapping()
            Dim tW_DateExport As String = Now.ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Dim oExp As New wExports

            oExp.W_SEToColumnPaymentSale()
            oExp.W_SEToColumnPaymentReturn()

            oExp.W_GETDataGrid(tW_DateExport)
            oExp.W_GETDataGridReturn(tW_DateExport)

            oExp.W_EXPxProcessApi()
            oExp.W_SETxWriteSuccess()

            cExportSale.nC_CreateFile = 0

        Catch ex As Exception

        End Try
    End Sub

    Public Sub C_CALxFindTable()
        Try
            Dim oSQL As New StringBuilder
            Dim oDatabase As New cDatabaseLocal

            oSQL.Clear()
            oSQL.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects")
            oSQL.AppendLine("WHERE object_id = OBJECT_ID(N'TCNTPdtRunning') AND type in (N'U'))")
            oSQL.AppendLine("BEGIN")
            oSQL.AppendLine("CREATE TABLE [dbo].[TCNTPdtRunning]")
            oSQL.AppendLine("([FNNum] [int] IDENTITY(1,1) NOT NULL,")
            oSQL.AppendLine("[FTRunningNumber] [varchar](50) NULL")
            oSQL.AppendLine(")")
            oSQL.AppendLine("END")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)


        Catch ex As Exception

        End Try
    End Sub

    Private Sub W_GENxCrtMapping()
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oSQL As New StringBuilder
            oSQL.Clear()
            oSQL.AppendLine(" IF NOT EXISTS (SELECT * FROM sys.objects")
            oSQL.AppendLine(" WHERE object_id = OBJECT_ID(N'TLNKMappingBranch') AND type in (N'U'))")
            oSQL.AppendLine(" BEGIN ")
            oSQL.AppendLine(" CREATE TABLE [dbo].[TLNKMappingBranch] ")
            oSQL.AppendLine(" ([FTBchCode] [VARCHAR](50) NULL,")
            oSQL.AppendLine(" [FTBchName] [VARCHAR](150) NULL,")
            oSQL.AppendLine(" [FTDefaultValue] [VARCHAR](20) NULL,")
            oSQL.AppendLine(" [FTUrsValue] [VARCHAR](20) NULL) ")
            oSQL.AppendLine(" INSERT [dbo].[TLNKMappingBranch] ")
            oSQL.AppendLine(" ([FTBchCode], ")
            oSQL.AppendLine(" [FTBchName],")
            oSQL.AppendLine(" [FTDefaultValue],")
            oSQL.AppendLine(" [FTUrsValue]) ")
            oSQL.AppendLine(" SELECT FTBchCode, FTBchName, ")
            oSQL.AppendLine(" (CASE  WHEN FTBchCode = '001'  THEN '01' ")
            oSQL.AppendLine(" WHEN FTBchCode = '002' THEN '03' ")
            oSQL.AppendLine(" WHEN FTBchCode = '003' THEN '04' ")
            oSQL.AppendLine(" WHEN FTBchCode = '004' THEN '02'")
            oSQL.AppendLine(" ELSE '01' END) AS FTDefaultValue ,")
            oSQL.AppendLine(" (CASE  WHEN FTBchCode = '001'  THEN '01' ")
            oSQL.AppendLine(" WHEN FTBchCode = '002' THEN '03' ")
            oSQL.AppendLine(" WHEN FTBchCode = '003' THEN '04' ")
            oSQL.AppendLine(" WHEN FTBchCode = '004' THEN '02' ")
            oSQL.AppendLine(" ELSE '01' END) AS FTUrsValue ")
            oSQL.AppendLine(" FROM TCNMBranch")
            oSQL.AppendLine(" END ")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)
            W_GENxCrtPayMapping()

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Create Table การชำระเงิน
    ''' </summary>
    Public Sub W_GENxCrtPayMapping()
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oSQL As New StringBuilder
            oSQL.Clear()
            oSQL.AppendLine(" IF NOT EXISTS (SELECT * FROM sys.objects ")
            oSQL.AppendLine(" WHERE object_id = OBJECT_ID(N'TLNKMappingPayment') AND type in (N'U')) ")
            oSQL.AppendLine(" BEGIN")
            oSQL.AppendLine(" CREATE TABLE [dbo].[TLNKMappingPayment] ")
            oSQL.AppendLine(" ([FTRcvCode] [VARCHAR](50) NULL, ")
            oSQL.AppendLine(" [FTRcvName] [VARCHAR](150) NULL, ")
            oSQL.AppendLine(" [FTPayDefaultValue] [VARCHAR](20) NULL,")
            oSQL.AppendLine(" [FTPayUrsValue] [VARCHAR](20) NULL) ")
            oSQL.AppendLine(" INSERT [dbo].[TLNKMappingPayment] ")
            oSQL.AppendLine(" ([FTRcvCode],[FTRcvName],[FTPayDefaultValue],[FTPayUrsValue]) ")
            oSQL.AppendLine(" SELECT FTRcvCode, FTRcvName,")
            oSQL.AppendLine(" (CASE WHEN FTRcvCode = '001' THEN 'CASH' ")
            oSQL.AppendLine("  WHEN FTRcvCode = '002' THEN 'CREDIT' ")
            oSQL.AppendLine("  WHEN FTRcvCode = '005' THEN 'TRANSFER' ")
            oSQL.AppendLine("  ELSE '' End) As FTPayDefaultValue,")
            oSQL.AppendLine("  (CASE   WHEN FTRcvCode = '001' THEN 'CASH'")
            oSQL.AppendLine("  WHEN FTRcvCode = '002' THEN 'CREDIT'")
            oSQL.AppendLine("  WHEN FTRcvCode = '005' THEN 'TRANSFER'")
            oSQL.AppendLine("  ELSE '' End) As FTPayUrsValue FROM TPSMRcv")
            oSQL.AppendLine(" END ")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)
        Catch ex As Exception

        End Try
    End Sub

End Class
