﻿Imports System.Net.Http
Imports Newtonsoft.Json

Public Class cmlExpSale

    <JsonProperty("docNo")>
    Property tDocNo As String

    <JsonProperty("docDate")>
    Property tDocDate As String

    <JsonProperty("salePoint")>
    Property tSalePoint() As String

    <JsonProperty("saleCode")>
    Property tSaleCode() As String

    <JsonProperty("saleName")>
    Property tSaleName() As String

    <JsonProperty("customerCode")>
    Property tCustomerCode() As String

    <JsonProperty("customerName")>
    Property tCustomerName() As String

    <JsonProperty("customerAddress")>
    Property tCustomerAddress() As String

    <JsonProperty("customerTaxId")>
    Property tCustomerTaxId() As String

    <JsonProperty("paymentType")>
    Property tPaymentType As String

    <JsonProperty("receiveBy")>
    Property tReceiveBy() As String

    <JsonProperty("receiveByPosition")>
    Property tReceiveByPosition() As String

    <JsonProperty("creditFee")>
    Property tCreditFee() As String

    <JsonProperty("discount")>
    Property tDiscount() As String

    <JsonProperty("total")>
    Property tTotal() As String

    <JsonProperty("item")>
    Property oItem() As List(Of cmlExpSalDT)


End Class
