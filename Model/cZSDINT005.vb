﻿Public Class cZSDINT005
    Property oDocNo As List(Of cDocNo)
    Property oTransaction As List(Of cTransaction)
    Property oCustomerDetails As List(Of cCustomerDetails)
    Property oRetailLineItem As List(Of cRetailLineItem)
    Property oLineItemDiscount As List(Of cLineItemDiscount)
    Property oLineItemCommissi As List(Of cLineItemCommissi)
    Property oTransactExtensio As List(Of cTransactExtensio)
    Property oLineItemExtensio As List(Of cLineItemExtensio)
    Property oTender As List(Of cTender)
    Property oTenderR As List(Of cTender)
    Property oCreditCard As List(Of cCreditCard)
    Property oCreditCardR As List(Of cCreditCard)
End Class

Public Class cDocNo
    Property FTShdDocNo As String = ""
    Property FTShdPosCN As String = ""
End Class

Public Class cTransaction
    Property RETAILSTOREID As String = ""
    Property BUSINESSDAYDATE As String = ""
    Property TRANSACTIONTYPECODE As String = ""
    Property WORKSTATIONID As String = ""
    Property TRANSACTIONSEQUENCENUMBER As String = ""
    Property BEGINDATETIMESTAMP As String = ""
    Property ENDDATETIMESTAMP As String = ""
    Property OPERATORID As String = ""
    Property TRANSACTIONCURRENCY As String = ""
    Property TRANSACTIONCURRENCY_ISO As String = ""
    Property ZREFERENCE0 As String = ""
    Property ZREFERENCE1 As String = ""
    Property PARTNERID As String = ""
    Property FTShdDocNo As String = ""
    Property FCShdVat As Double = 0
End Class

Public Class cCustomerDetails
    Property RETAILSTOREID As String = ""
    Property BUSINESSDAYDATE As String = ""
    Property TRANSACTIONTYPECODE As String = ""
    Property WORKSTATIONID As String = ""
    Property TRANSACTIONSEQUENCENUMBER As String = ""
    Property CUSTOMERINFOTYPE As String = ""
    Property DATAELEMENTID As String = ""
    Property DATAELEMENTVALUE As String = ""
    Property FTShdDocNo As String = ""
End Class

Public Class cRetailLineItem
    Property RETAILSTOREID As String = ""
    Property BUSINESSDAYDATE As String = ""
    Property TRANSACTIONTYPECODE As String = ""
    Property WORKSTATIONID As String = ""
    Property TRANSACTIONSEQUENCENUMBER As String = ""
    Property RETAILSEQUENCENUMBER As String = ""
    Property RETAILTYPECODE As String = ""
    Property ITEMIDQUALIFIER As String = ""
    Property ITEMID As String = ""
    Property RETAILQUANTITY As String = ""
    Property SALESUNITOFMEASURE As String = ""
    Property SALESAMOUNT As String = ""
    Property NORMALSALESAMOUNT As String = ""
    Property SERIALNUMBER As String = ""
    Property PROMOTIONID As String = ""
    Property ACTUALUNITPRICE As String = ""
    Property UNITS As String = ""
    Property FCShdVatRate As Double = 0.0
    Property FTShdDocType As String = ""
    Property FTShdDocNo As String = ""
End Class

Public Class cLineItemDiscount
    Property RETAILSTOREID As String = ""
    Property BUSINESSDAYDATE As String = ""
    Property TRANSACTIONTYPECODE As String = ""
    Property WORKSTATIONID As String = ""
    Property TRANSACTIONSEQUENCENUMBER As String = ""
    Property RETAILSEQUENCENUMBER As String = ""
    Property DISCOUNTSEQUENCENUMBER As String = ""
    Property DISCOUNTTYPECODE As String = ""
    Property REDUCTIONAMOUNT As String = ""
    Property FCShdVatRate As Double = 0.0
    Property FTShdDocType As String = ""
    Property FTShdDocNo As String = ""
End Class

Public Class cLineItemCommissi
    Property RETAILSTOREID As String = ""
    Property BUSINESSDAYDATE As String = ""
    Property TRANSACTIONTYPECODE As String = ""
    Property WORKSTATIONID As String = ""
    Property TRANSACTIONSEQUENCENUMBER As String = ""
    Property COMMISIONSEQUENCENUMBER As String = ""
    Property COMMEMPLOYEEQUAL As String = ""
    Property COMMISSIONEMPLOYEEID As String = ""
    Property COMMISSIONAMOUNT As String = ""
    Property FTShdDocNo As String = ""
End Class

Public Class cTransactExtensio
    Property RETAILSTOREID As String = ""
    Property BUSINESSDAYDATE As String = ""
    Property TRANSACTIONTYPECODE As String = ""
    Property WORKSTATIONID As String = ""
    Property TRANSACTIONSEQUENCENUMBER As String = ""
    Property FIELDGROUP As String = ""
    Property FIELDNAME As String = ""
    Property FIELDVALUE As String = ""
    Property FTShdDocNo As String = ""
End Class

Public Class cLineItemExtensio
    Property RETAILSTOREID As String = ""
    Property BUSINESSDAYDATE As String = ""
    Property TRANSACTIONTYPECODE As String = ""
    Property WORKSTATIONID As String = ""
    Property TRANSACTIONSEQUENCENUMBER As String = ""
    Property RETAILSEQUENCENUMBER As String = ""
    Property FIELDGROUP As String = ""
    Property FIELDNAME As String = ""
    Property FIELDVALUE As String = ""
    Property FTShdDocNo As String = ""
End Class

Public Class cTender
    Property RETAILSTOREID As String = ""
    Property BUSINESSDAYDATE As String = ""
    Property TRANSACTIONTYPECODE As String = ""
    Property WORKSTATIONID As String = ""
    Property TRANSACTIONSEQUENCENUMBER As String = ""
    Property TENDERSEQUENCENUMBER As String = ""
    Property TENDERTYPECODE As String = ""
    Property TENDERAMOUNT As String = ""
    Property TENDERCURRENCY As String = ""
    Property TENDERID As String = ""
    Property ACCOUNTNUMBER As String = ""
    Property REFERENCEID As String = ""
    Property FTEdcOther As String = ""
    Property FTShdDocNo As String = ""
    Property FTRcvCode As String = ""
End Class

Public Class cCreditCard
    Property RETAILSTOREID As String = ""
    Property BUSINESSDAYDATE As String = ""
    Property TRANSACTIONTYPECODE As String = ""
    Property WORKSTATIONID As String = ""
    Property TRANSACTIONSEQUENCENUMBER As String = ""
    Property PAYMENTCARD As String = ""
    Property CARDNUMBER As String = ""
    Property CARDNUMBERSUFFIX As String = ""
    Property CARDEXPIRATIONDATE As String = ""
    Property CARDHOLDERNAME As String = ""
    Property ADJUDICATIONCODE As String = ""
    Property AUTHORIZINGTERMID As String = ""
    Property AUTHORIZATIONDATETIME As String = ""
    Property MEDIAISSUERID As String = ""
    Property FTShdDocNo As String = ""
    Property TENDERSEQUENCENUMBER As String = ""
    Property FNEdcPort As String = ""
    Property FTRcvCode As String = ""
End Class