﻿Imports Newtonsoft.Json

Public Class cmlExpSalDT

    <JsonProperty("itemCode")>
    Public Property tItemCode() As String

    <JsonProperty("itemName")>
    Public Property tItemName() As String

    <JsonProperty("price")>
    Public Property tPrice() As String

    <JsonProperty("qty")>
    Public Property tQty() As String
End Class
