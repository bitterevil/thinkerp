﻿Public Class cTCNTPdtAjpHD
    Public Property FTIphDocNo() As String
    Public Property FTIphDocType() As String
    Public Property FDIphDocDate() As String
    Public Property FTIphDocTime() As String
    Public Property FTBchCode() As String
    Public Property FTDptCode() As String
    Public Property FTUsrCode() As String
    Public Property FTIphApvCode() As String
    Public Property FCIphTotal() As Double
    Public Property FTIphStaDoc() As String
    Public Property FTIphStaPrcDoc() As String
    Public Property FNIphStaDocAct() As Long
    Public Property FTIphRmk() As String
    Public Property FTSplCode() As String
    Public Property FTCstCode() As String
    Public Property FTIphAdjType() As String
    Public Property FDIphAffect() As String
    Public Property FTIphStaPrcPri() As String
    Public Property FTIphBchTo() As String
    Public Property FTIphZneTo() As String
    Public Property FDDateUpd() As String
    Public Property FTTimeUpd() As String
    Public Property FTWhoUpd() As String
    Public Property FDDateIns() As String
    Public Property FTTimeIns() As String
    Public Property FTWhoIns() As String
    Public Property FTIphPriType() As String
    Public Property FDIphDStop() As String

End Class
