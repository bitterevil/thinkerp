﻿Public Class cGetFormatAutoTemplate
    Private _FTSatStaResetBill As String
    Private _FTSatRetFmtChar As String
    Private _FTSatRetFmtYear As String
    Private _FTSatRetFmtMonth As String
    Private _FTSatRetFmtDay As String
    Private _FTSatRetFmtNum As String
    Private _FTSatRetFmtAll As String
    Private _FTBchCode As String
    Private _FTPKField As String
    Private _FTTableName As String
    Private _FTDocType As String
    Private _FTSatDocTypeName As String

    Public Property FTSatStaResetBill() As String
        Get
            Return Me._FTSatStaResetBill
        End Get
        Set(ByVal value As String)
            If ((Me._FTSatStaResetBill = value) _
               = False) Then
                Me._FTSatStaResetBill = value
            End If
        End Set
    End Property

    Public Property FTSatRetFmtChar() As String
        Get
            Return Me._FTSatRetFmtChar
        End Get
        Set(ByVal value As String)
            If ((Me._FTSatRetFmtChar = value) _
               = False) Then
                Me._FTSatRetFmtChar = value
            End If
        End Set
    End Property

    Public Property FTSatRetFmtYear() As String
        Get
            Return Me._FTSatRetFmtYear
        End Get
        Set(ByVal value As String)
            If ((Me._FTSatRetFmtYear = value) _
               = False) Then
                Me._FTSatRetFmtYear = value
            End If
        End Set
    End Property

    Public Property FTSatRetFmtMonth() As String
        Get
            Return Me._FTSatRetFmtMonth
        End Get
        Set(ByVal value As String)
            If ((Me._FTSatRetFmtMonth = value) _
               = False) Then
                Me._FTSatRetFmtMonth = value
            End If
        End Set
    End Property

    Public Property FTSatRetFmtDay() As String
        Get
            Return Me._FTSatRetFmtDay
        End Get
        Set(ByVal value As String)
            If ((Me._FTSatRetFmtDay = value) _
               = False) Then
                Me._FTSatRetFmtDay = value
            End If
        End Set
    End Property

    Public Property FTSatRetFmtNum() As String
        Get
            Return Me._FTSatRetFmtNum
        End Get
        Set(ByVal value As String)
            If ((Me._FTSatRetFmtNum = value) _
               = False) Then
                Me._FTSatRetFmtNum = value
            End If
        End Set
    End Property

    Public Property FTSatRetFmtAll() As String
        Get
            Return Me._FTSatRetFmtAll
        End Get
        Set(ByVal value As String)
            If ((Me._FTSatRetFmtAll = value) _
               = False) Then
                Me._FTSatRetFmtAll = value
            End If
        End Set
    End Property

    Public Property FTBchCode() As String
        Get
            Return Me._FTBchCode
        End Get
        Set(ByVal value As String)
            If ((Me._FTBchCode = value) _
               = False) Then
                Me._FTBchCode = value
            End If
        End Set
    End Property

    Public Property FTPKField() As String
        Get
            Return Me._FTPKField
        End Get
        Set(ByVal value As String)
            If ((Me._FTPKField = value) _
               = False) Then
                Me._FTPKField = value
            End If
        End Set
    End Property

    Public Property FTTableName() As String
        Get
            Return Me._FTTableName
        End Get
        Set(ByVal value As String)
            If ((Me._FTTableName = value) _
               = False) Then
                Me._FTTableName = value
            End If
        End Set
    End Property

    Public Property FTDocType() As String
        Get
            Return Me._FTDocType
        End Get
        Set(ByVal value As String)
            If ((Me._FTDocType = value) _
               = False) Then
                Me._FTDocType = value
            End If
        End Set
    End Property

    Public Property FTSatDocTypeName() As String
        Get
            Return Me._FTSatDocTypeName
        End Get
        Set(ByVal value As String)
            If ((Me._FTSatDocTypeName = value) _
               = False) Then
                Me._FTSatDocTypeName = value
            End If
        End Set
    End Property
End Class
