﻿Imports System.Threading

Namespace My

    ' The following events are available for MyApplication:
    ' 
    ' Startup: Raised when the application starts, before the startup form is created.
    ' Shutdown: Raised after all application forms are closed.  This event is not raised if the application terminates abnormally.
    ' UnhandledException: Raised if the application encounters an unhandled exception.
    ' StartupNextInstance: Raised when launching a single-instance application and the application is already active. 
    ' NetworkAvailabilityChanged: Raised when the network connection is connected or disconnected.
    Partial Friend Class MyApplication

        Private Declare Auto Function AllocConsole Lib "kernel32.dll" () As Boolean
        Private oDatabase As cDatabaseLocal
        Private oExportAuto As cExportAuto
        '   Private oPurgeAuto As cPurgeAuto

        Private Sub MyApplication_Startup(sender As Object, e As ApplicationServices.StartupEventArgs) Handles Me.Startup
            Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-GB")
            C_CALxMain()
        End Sub

        Public Sub C_CALxMain()
            Dim aArgs() As String = Environment.GetCommandLineArgs()
            If aArgs.Length > 1 Then
                'Get Parameter
                Dim bExport As Boolean = False

                If aArgs(1).ToLower = "-export" Then
                    bExport = True
                End If

                'If aArgs.Length = 3 AndAlso bExport Then  'PAN การ Export เฉพาะ  2016-06-09 
                '        Select Case aArgs(2).ToLower
                '            Case cCNVB.tVB_ZSDINT005
                '                cCNVB.tVB_Export = cCNVB.tVB_ZSDINT005
                '            Case cCNVB.tVB_ZSDINT006
                '                cCNVB.tVB_Export = cCNVB.tVB_ZSDINT006
                '            Case cCNVB.tVB_ZGLINT001
                '                cCNVB.tVB_Export = cCNVB.tVB_ZGLINT001
                '            Case cCNVB.tVB_ZGLINT002
                '                cCNVB.tVB_Export = cCNVB.tVB_ZGLINT002
                '        End Select
                '    End If


                If bExport = True Then
                    cCNVB.bVB_Auto = True 'Set Status Auto Global
                    'Not use Console
                    'AllocConsole()

                    'Set Culture
                    Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-GB")

                    cLog.C_CALxWriteLogAuto("Start...")

                    'Default Eng
                    cCNVB.nVB_CutLng = 2
                    cCNSP.SP_LNGxSetDefAftChg()

                    Try
                        AdaConfig.cConfig.C_GETxConfigXml()
                        AdaConfig.cConfig.C_GETxConfigXmlAPI()

                        'Check Connection
                        If AdaConfig.cConfig.C_CHKbSQLSourceConnect = True Then

                            oDatabase = New cDatabaseLocal
                            If cApp.C_CALaCheckTableLog.Count > 0 Then
                                cLog.C_CALxWriteLogAuto(cCNSP.SP_STRtToken(cCNMS.tMS_CN110, ";", cCNVB.nVB_CutConst, True))
                                End
                            End If
                            cCNSP.SP_GETxVariable()

                        Else
                            Throw New Exception(cCNSP.SP_STRtToken(cCNMS.tMS_CN004, ";", cCNVB.nVB_CutConst, True))
                        End If

                        'Create Table Log
                        oDatabase.C_CALnExecuteNonQuery(cApp.tSQLCmdCheckTableTemp)

                        'perform auto

                        Select Case True
                            Case bExport
                                C_CALxExportAuto()
                        End Select


                    Catch ex As Exception
                        cLog.C_CALxWriteLogAuto(ex.Message)
                        Thread.Sleep(1000 * 10)
                        End
                    End Try

                    'Console.ReadKey()
                End If
            End If

        End Sub

        Private Sub C_CALxExportAuto()
            'Export Auto
            Dim dStartTime As Date = Now
            'Console.WriteLine("Export Start...")
            cLog.C_CALxWriteLogAuto("Export Auto Start [Start : " & Format(dStartTime, "HH:mm:ss") & "]...")
            oExportAuto = New cExportAuto
            oExportAuto.C_CALxProcess()

            Dim dEndTime As Date = Now
            Dim dDiffTime As TimeSpan = dEndTime - dStartTime
            'Console.WriteLine("Export Stop...")
            cLog.C_CALxWriteLogAuto("Export Auto Stop [End : " & Format(dEndTime, "HH:mm:ss") & "],[Duration : " & FormatNumber(dDiffTime.TotalMinutes, 2) & " minutes.]...")
            Thread.Sleep(1000 * 10)
            End
        End Sub

    End Class

End Namespace

